#ifndef __MESSAGE_QUEUE_H__
#define __MESSAGE_QUEUE_H__

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define THREAD_COUNT 3
#define DELETE_THREAD_COUNT 1
#define DATA_MAX_COUNT 1 

typedef struct qNode
{
	int data;      
	int mEnqueueThreadId;  
	struct qNode* prev; 
	struct qNode* next;
}qNode;

typedef struct qQueue
{
	qNode* head;    
	qNode* tail;    
	int count;  
}qQueue;

typedef struct qArgPthread
{
	qQueue* mQueue;      
	int mThreadId;  
} qArgPthread;

void qInitQueue(qQueue* aQueue);
void *qEnqueue(void* aArgPthread);
void *qDequeue(void* aArgPthread);

pthread_mutex_t mutex;
pthread_mutex_t mutexForAdd[THREAD_COUNT];
pthread_mutex_t mutexForDel[DELETE_THREAD_COUNT];

pthread_cond_t d_cond;
pthread_cond_t a_cond;

#endif
