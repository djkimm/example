#include "Message_Queue.h"

/* Initialize Queue */
void qInitQueue(qQueue* aQueue)
{
	aQueue->head  = NULL;
	aQueue->tail  = NULL;
	aQueue->count = 0;
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: queue is Initialized. |\n");
	printf("+-----------------------------------+\n\n");
}

/* Enqueue */
void  *qEnqueue(void* aArgPthread)
{
	qArgPthread* sArgPthread   = NULL;
	qQueue* sQueue = NULL;
	qNode* newNode = NULL;
	
	int sInputData=0;
	int sRepeatCount=0;
	int sThreadId=0;
	
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue = sArgPthread->mQueue;
	sThreadId = sArgPthread->mThreadId;
	
	printf("[#%3d][ENQUEUE_THREAD] is ready.\n",sThreadId);
	
	while(sRepeatCount < DATA_MAX_COUNT)
	{
		pthread_mutex_lock(&mutexForAdd[sThreadId]);
		newNode = (qNode *)malloc( sizeof(qNode) );
		if(sQueue->count >= THREAD_COUNT)
		{
			pthread_cond_wait(&d_cond,&mutexForAdd[sThreadId]);
		}
		else
		{
			sInputData=rand()% 90+10;
			newNode->data = sInputData;
			newNode->next = NULL;
			newNode->mEnqueueThreadId = sThreadId;
			printf("[#%3d][ENQUEUE_THREAD] Enqueue DATA : %d QUEUE STATUS : [ %d/%d ]\n",
					sThreadId,
					sInputData,
					sQueue->count+1,
					THREAD_COUNT);
			if(sQueue->count==0)
			{
				sQueue->head   = newNode;
				sQueue->tail = newNode;
				newNode->next = NULL;
				newNode->prev=NULL;
			}
			else
			{
				sQueue->tail->next = newNode;
				newNode->next=NULL;
				newNode->prev = sQueue->tail;
	
			}
			sQueue->tail  = newNode;
			sQueue->count++;
			sRepeatCount++;
		}
		pthread_cond_signal(&a_cond);
		/* Enqueue Mutex Unlock */
		pthread_mutex_unlock( &mutexForAdd[sThreadId] );
	}
}

/* Dequeue */
void* qDequeue(void* aArgPthread)
{
	qArgPthread *sArgPthread = NULL;
	qQueue *sQueue=NULL;
	int sOutputData=0;
	int sRepeatCount=0;
	int sThreadId=0;
	int sEnqueueThreadId=0;
	sArgPthread = (qArgPthread*)aArgPthread;
	sQueue = sArgPthread->mQueue;
	sThreadId = sArgPthread->mThreadId;
	
	printf("[#%3d][DEQUEUE_THREAD] is ready.\n", sThreadId);
	
	while(sRepeatCount < DATA_MAX_COUNT*THREAD_COUNT)
	{
		pthread_mutex_lock(&mutexForDel[sThreadId]);
		if(sQueue->count == 0)
		{
			pthread_cond_wait(&a_cond,&mutexForDel[sThreadId]);
		}
		else
		{
			sOutputData = sQueue->head->data;
			sEnqueueThreadId  = sQueue->head->mEnqueueThreadId;
			printf("[#%3d][DEQUEUE_THREAD] Dequeue DATA : %d QUEUE STATUS : [ %d/%d ] data by [#%3d][ENQUEUE_THREAD]\n",
					sThreadId,
					sOutputData,
					sQueue->count-1,
					THREAD_COUNT,
					sEnqueueThreadId);
			if(sQueue->count == 1)
			{
				free(sQueue->head);
				sQueue->head = NULL;
				sQueue->tail = NULL;
				sQueue->count = 0;
			}
			else
			{
				sQueue->head = sQueue->head->next;
				free(sQueue->head->prev);
				sQueue->head->prev = NULL;
				sQueue->count--;
			}
			sRepeatCount++;
		}
		pthread_cond_signal(&d_cond);
		pthread_mutex_unlock(&mutexForDel[sThreadId]);
	}
}
