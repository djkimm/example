#include "Message_Queue.h"
int main()
{
	pthread_t sEnqueueThread[THREAD_COUNT];    
	pthread_t sDequeueThread[DELETE_THREAD_COUNT];
	qArgPthread* sArgEnqueueThread[THREAD_COUNT];   
	qArgPthread* sArgDequeueThread[DELETE_THREAD_COUNT];
	
	qQueue* sQueue=NULL;
	
	int sIsCreatedThread=0;    
	int sIsJoinedThread=0;   
	int sReturnValue=0;  
	int sCountThread=0;       
	
	sQueue=(qQueue*)malloc(sizeof(qQueue));
	pthread_mutex_init(&mutex,NULL);
	pthread_cond_init(&d_cond,NULL);
	pthread_cond_init(&a_cond,NULL);
	for(sCountThread=0;sCountThread<THREAD_COUNT;sCountThread++)
	{
		pthread_mutex_init(&mutexForAdd[sCountThread],NULL);
	}
	for(sCountThread=0;sCountThread<DELETE_THREAD_COUNT;sCountThread++)
	{
		pthread_mutex_init(&mutexForDel[sCountThread],NULL);
	}
	qInitQueue(sQueue);     

	pthread_mutex_lock(&mutex);

	/* Enqueue Thread Create */
	for(sCountThread = 0; sCountThread<THREAD_COUNT; sCountThread++)
	{
		sArgEnqueueThread[sCountThread] = (qArgPthread *)malloc(sizeof(qArgPthread));
		sArgEnqueueThread[sCountThread]->mQueue=sQueue;
		sArgEnqueueThread[sCountThread]->mThreadId=sCountThread;
		
		sIsCreatedThread = pthread_create(&sEnqueueThread[sCountThread],
				NULL,
				&qEnqueue,
				(void *)sArgEnqueueThread[sCountThread]);
		
		if( sIsCreatedThread != 0 )
		{
			printf("[ERROR-001] :: cannot create Addthread.\n");
		}
	}
																		      
	/* Dequeue Thread Create*/
	for(sCountThread=0; sCountThread<DELETE_THREAD_COUNT;sCountThread++)
	{
		sArgDequeueThread[sCountThread] = (qArgPthread *)malloc(sizeof(qArgPthread));
		sArgDequeueThread[sCountThread]->mQueue=sQueue;
		sArgDequeueThread[sCountThread]->mThreadId=sCountThread;
		
		sIsCreatedThread = pthread_create(&sDequeueThread[sCountThread],
				NULL,
				&qDequeue,
				(void *)sArgDequeueThread[sCountThread]);
		if( sIsCreatedThread != 0 )
		{
			printf("[ERROR-001] :: cannot create Delete thread.\n");
		}
	}
	pthread_mutex_unlock(&mutex);
   
	/* Thread Join*/
	for(sCountThread=0;sCountThread<THREAD_COUNT;sCountThread++)
	{
		/* Enqueue 스레드 */
		sIsJoinedThread = pthread_join(sEnqueueThread[sCountThread],
				0);
		if(sIsJoinedThread != 0)
		{
			printf("[ERROR-002] :: cannot join enqueue thread.\n");
		}
	}
	for(sCountThread = 0;sCountThread<DELETE_THREAD_COUNT;sCountThread++)  
	{ 
		/* Dequeue 스레드 */
		sIsJoinedThread = pthread_join(sDequeueThread[sCountThread],
				0);
		
		if( sIsJoinedThread != 0 )
		{
			printf("[ERROR-002] :: cannot join dequeue thread.\n");
		}
	}
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: program is Exited.... |\n");
	printf("+-----------------------------------+\n\n");
	return 0;
}

