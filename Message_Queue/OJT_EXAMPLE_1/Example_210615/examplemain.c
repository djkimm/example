#include "example.h"

int main(void){

			int menu = 0;
			int data,n;
			int number;
			int check;

	        queue_t* queue = Init();
			
			void print_Menu(){

			printf("1.Add Node(Last position)\n");

			printf("2.First Node Delete\n");

			printf("3.Data Search\n");

			printf("4.List Print\n");

			printf("5.Menu Print\n");
			printf("6.Quit\n");
			}

			print_Menu();

			while(menu!=6){
				printf("선택:");
				check = scanf("%d",&menu);
				
				while(getchar() !='\n');
				if(check==0) {
				printf("Only Insert Int data(1~6)\n\n");
					continue;
				}
									
				switch(menu){
					case 1:
						printf("\n삽입할 원소값:");
						check=scanf("%d",&data);		

						while(getchar() !='\n');
						if(check==0){	
							printf("Invalid data.\n\n");
							continue;
						}
						add_Node(queue,data);
						break;
					case 2:
						printf("\nNode Delete\n");
						delete_First_Node(queue);
						break;
					case 3:
						printf("\n데이터 검색: ");
					    check=scanf("%d",&number);
						while(getchar() != '\n');
						if(check==0){
							printf("Invalid data\n\n");
							continue;
						}
						node_Search(queue,number);
						break;
					case 4:
						printf("\n리스트 전체 출력\n");
						print_List(queue);
						break;
					case 5:
						printf("\nMenu Print\n\n");
						print_Menu();
					case 6:
						break;
						
					default:
						printf("\n잘못 선택\n\n");
			
				}
			}

}
