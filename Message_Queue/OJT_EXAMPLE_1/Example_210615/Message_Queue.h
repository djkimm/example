#ifndef __MAKE_QU_EUE_H__
#define __MAKE_QU_EUE_H__

#define THREAD_COUNT 3
#define DATA_MAX_COUNT 20

#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>

typedef struct _node_s{
	int data;
	struct _node_s* next;
	struct _node_s* prev;
}qNode;

typedef struct queue_t{
	Node* head;
	Node* tail;
	int count;
}qQueue;

typedef struct qArgPthread
{
	qQueue* mQueue;
	int mThreadId;
}qArgPthread;

void qInitQueue(qQueue* aQueue);

void add_Node(void* aArgPthread);

void delete_First_Node(void* aArgPthread);

/* Global Mutex & Condition Variable */
pthread_mutex_t  gMutexForThread;
pthread_mutex_t  gMutexForEnqDeq[THREAD_COUNT];

pthread_cond_t   gCondition[THREAD_COUNT];

/* Flag Variables */
int  gMainFlag;
int  gFlagForThread[THREAD_COUNT];
int  gFlagForEnqDeq[THREAD_COUNT];

#endif
