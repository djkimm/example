#define ADD_THREAD_COUNT 2
#define DELETE_THREAD_COUNT 2
#define DATA_MAX_COUNT 20

#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>

typedef struct _node_s{
	int mData;
	int mEnqueueThreadId; /*데이터를 준 Thread Id*/
	struct _node_s* mNextNode;
	struct _node_s* mPrevNode;
}qNode;

typedef struct queue_t{
	qNode* mFrontNode;
	qNode* mRearNode;
	int mCount;
}qQueue;

typedef struct qArgPthread
{
	qQueue* mQueue;
	int mThreadId;
}qArgPthread;

void qInitQueue(qQueue *aQueue);
void* add_Node(void* aArgPthread);
//void* delete_Node(void* aArgPthread);

pthread_mutex_t gMutexForThread;
pthread_mutex_t gMutexForEnqDeq[ADD_THREAD_COUNT];

pthread_cond_t gCondition[ADD_THREAD_COUNT];

int gMainFlag;
int gFlagForThread[ADD_THREAD_COUNT];
int gFlagForEnqDeq[ADD_THREAD_COUNT];
int main()
{
	pthread_t sEnqueueThread[ADD_THREAD_COUNT];
	pthread_t sDequeueThread[DELETE_THREAD_COUNT];

	qArgPthread* sArgEnqueueThread[ADD_THREAD_COUNT];
	qArgPthread* sArgDequeueThread[DELETE_THREAD_COUNT];

	qQueue* sQueue = NULL;

	int sIsCreatedThread = 0;
	int sIsJoinedThread = 0;
	int sReturnValue = 0;
	int sAddCountThread = 0;
	int sDelCountThread = 0;
	int sDoubleThreadCount=0;
	
	sQueue = (qQueue*)malloc(sizeof(qQueue));

	sDoubleThreadCount = ADD_THREAD_COUNT*2;

	pthread_mutex_init(&gMutexForThread, NULL);

	for(sAddCountThread=0; sAddCountThread <ADD_THREAD_COUNT; sAddCountThread++)
	{
		pthread_mutex_init(&gMutexForEnqDeq[sAddCountThread],NULL);
		pthread_cond_init(&gCondition[sAddCountThread],NULL);
		gFlagForThread[sAddCountThread]=0;
		gFlagForEnqDeq[sAddCountThread]=0;
	}
	gMainFlag = 0;
	qInitQueue(sQueue);

	pthread_mutex_lock(&gMutexForThread);

	for(sAddCountThread =0; sAddCountThread<ADD_THREAD_COUNT;sAddCountThread++)
	{
		sArgEnqueueThread[sAddCountThread] =(qArgPthread*)malloc(sizeof(qArgPthread));

		sArgEnqueueThread[sAddCountThread]->mQueue = sQueue;
		sArgEnqueueThread[sAddCountThread]->mThreadId = sAddCountThread;

		sIsCreatedThread = pthread_create(&sEnqueueThread[sAddCountThread],NULL,&add_Node,(void*)sArgEnqueueThread[sAddCountThread]);
		if(sIsCreatedThread !=0)
		{
			printf("[ERROR] :: cannot create thread.\n");
		}
	}

	while(gMainFlag < sDoubleThreadCount)
	{
		gMainFlag =0;
		for(sAddCountThread=0; sAddCountThread < ADD_THREAD_COUNT; sAddCountThread++)
		{
			gMainFlag += gFlagForThread[sAddCountThread];
		}
		sleep(0);
	}

	pthread_mutex_unlock(&gMutexForThread);

	for(sAddCountThread=0; sAddCountThread < ADD_THREAD_COUNT; sAddCountThread++)
	{
		sIsJoinedThread = pthread_join(sEnqueueThread[sAddCountThread],(void**)&sReturnValue);

		if(sIsJoinedThread !=0)
		{
			printf("[ERROR] :: cannot join enqueue Thread.\n");
		}
	}
	printf("\n |[SYSTEM] :: PROGRAM IS EXITED....|\n");

	return 0;
}


//List 초기화 함수
//============================================================
void qInitQueue(qQueue* aQueue)
{  
	aQueue->mFrontNode = NULL;
	aQueue->mRearNode = NULL;
	aQueue->mCount = 0;
}
//==================노드 추가 함수===========================
//================== 2. Tail에추가==========================
void *add_Node(void* aArgPthread)
{

	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	qNode       * sNewNode      = NULL;
	int           sInputData    = 0;
	int           sRepeatCount  = 0;
	int           sThreadId     = 0;
	
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue      = sArgPthread->mQueue;
	sThreadId   = sArgPthread->mThreadId;

	
	printf("[#%3d][ENQUEUE_THREAD] is ready.\n", sThreadId );
	
	gFlagForThread[sThreadId] += 1;
	
	while( sRepeatCount < DATA_MAX_COUNT )
	{
		while( gFlagForEnqDeq[sThreadId] == 0 ) 
		{
			sleep(0);
		}
		pthread_mutex_lock( &gMutexForThread );
		sNewNode = (qNode *)malloc( sizeof( qNode ) );
		if( sQueue->mCount >= ADD_THREAD_COUNT /* sMaxBuff */ )
		{
		}
		else
		{
			sInputData                  = rand() % 90 + 10;
			sNewNode->mData             = sInputData;
			sNewNode->mNextNode         = NULL;
			sNewNode->mEnqueueThreadId  = sThreadId;
			
			printf("[#%3d][ENQUEUE_THREAD] Enqueue DATA : %d( %d/%d ) QUEUE STATUS : [ %d/%d ]\n",
					sThreadId,
					
					sInputData,
					
					sRepeatCount+1,
					
					DATA_MAX_COUNT,
					
					sQueue->mCount+1,
					
					ADD_THREAD_COUNT );
			
			if( sQueue->mCount == 0 )
			
			{
				sQueue->mFrontNode   = sNewNode;
				sNewNode->mPrevNode  = NULL;
			}
			else
			{
				sQueue->mRearNode->mNextNode  = sNewNode; 
				sNewNode->mPrevNode           = sQueue->mRearNode;
			


			}


																																																		             
			sQueue->mRearNode  = sNewNode;
			

			
			sQueue->mCount++;


			
			sRepeatCount++;
			
		}


		

		
		pthread_mutex_unlock( &gMutexForThread );


		
		pthread_mutex_lock( &gMutexForEnqDeq[sThreadId] );


		
		gFlagForEnqDeq[sThreadId] = 0;
		

		

		
		pthread_cond_signal( &gCondition[sThreadId] );

		

		
		pthread_mutex_unlock( &gMutexForEnqDeq[sThreadId] );


		


		
	}
}








