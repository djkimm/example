#include "example2.h"

int main(void){

	pthread_t sEnqueueThread[THREAD_COUNT]; /*THREAD_COUNT 만큼의 ENQUEUE Thread 선언*/
	pthread_t sDequeueThread[THREAD_COUNT]; /*THREAD_COUNT 만큼의 DEQUEUE Thread 선언---> 변경해야할 사항 */

	qArgPthread* sArgEnqueueThread[THREAD_COUNT]; /*THREAD_COUNT만큼의 인자로 넘겨줄 구조체 */
	qArgPthread* sArgDequeueThread[THREAD_COUNT]; /*위와 같음 */

	sArgPthread = (qArgPthread *)aArgPthread;
	qQueue *sQueue = NULL;

	int sIsCreatedThread=0;
	int sIsJoinedThread=0;
	int sReturnValue=0;
	int sCountThread=0;
	int sDoubleThreadCount=0;

	int menu = 0;
	int data,n;
	int number;
	int check;

	while(menu!=3){
		printf("선택:");
		check = scanf("%d",&menu);
		while(getchar() !='\n');
		if(check==0) {
			printf("Only Insert Int data(1~6)\n\n");
			continue;
		}
		switch(menu){
			case 1:
				add_Node(&sArgPthread);
				break;
			case 2:
				printf("\nNode Delete\n");
				delete_First_Node(&sArgPthread);
				break;
			case 3:
				break;
			default:
				printf("\n잘못 선택\n\n");
		}
	}



	sQueue =(qQueue*)malloc(sizeof(qQueue));
	sDoubleThreadCount = THREAD_COUNT *2;

	pthread_mutex_init(&gMutexForThread,NULL);
	
	for(sCountThread=0; sCountThread <THREAD_COUNT; sCountThread++)
	{
		pthread_mutex_init(&gMutexForEnqDeq[sCountThread],NULL);
		pthread_cond_init(&gCondition[sCountThread],NULL);
		gFlagForThread[sCountThread]=0;
		gFlagForEnqDeq[sCountThread]=0;
	}
	gMainFlag =0;

	qInitQueue(sQueue); /*sQueue Initialize*/

	pthread_mutex_lock(&gMutexForThread);

	for(sCountThread =0; sCountThread<THREAD_COUNT;sCountThread++)
	{
		sArgEnqueueThread[sCountThread] =(qArgPthread*)malloc(sizeof(qArgPthread));

		sArgEnqueueThread[sCountThread]->mQueue = sQueue;
		sArgEnqueueThread[sCountThread]->mThreadId = sCountThread;

		sIsCreatedThread = pthread_create(&sEnqueueThread[sCountThread],NULL,&add_Node,(void*)sArgEnqueueThread[sCountThread]);
		if(sIsCreatedThread !=0)
		{
			printf("[ERROR] :: cannot create thread.\n");
		}
	}

	for(sCountThread =0; sCountThread<THREAD_COUNT;sCountThread++)
	{
		sArgEnqueueThread[sCountThread] =(qArgPthread*)malloc(sizeof(qArgPthread));

		sArgEnqueueThread[sCountThread]->mQueue = sQueue;
		sArgEnqueueThread[sCountThread]->mThreadId = sCountThread;

		sIsCreatedThread = pthread_create(&sEnqueueThread[sCountThread],NULL,&delete_First_Node,(void*)sArgEnqueueThread[sCountThread]);
		if(sIsCreatedThread !=0)
		{
			printf("[ERROR] :: cannot create thread.\n");
		}
	}


	while(gMainFlag < sDoubleThreadCount)
	{
		gMainFlag =0;
		for(sCountThread=0; sCountThread < THREAD_COUNT; sCountThread++)
		{
			gMainFlag += gFlagForThread[sCountThread];
		}
		sleep(0);
	}

	pthread_mutex_unlock(&gMutexForThread);

	for(sCountThread=0; sCountThread < THREAD_COUNT; sCountThread++)
	{
		sIsJoinedThread = pthread_join(sEnqueueThread[sCountThread],(void**)&sReturnValue);

		if(sIsJoinedThread !=0)
		{
			printf("[ERROR] :: cannot join enqueue Thread.\n");
		}
		
		sIsJoinedThread = pthread_join(sDequeueThread[sCountThread],(void**)&sReturnValue);

		if(sIsJoinedThread !=0)
		{
			printf("[ERROR] :: cannot join dequeue Thread.\n");
		}
	}
	printf("\n |[SYSTEM] :: PROGRAM IS EXITED....|\n");

	return 0;
}

