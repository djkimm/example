#ifndef __EXAMPLE_H__
#define __EXAMPLE_H__

#define MAX_SIZE 20
#define THREAD_COUNT 1

#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>

typedef struct _node_s{
	int data;
	int mEnqueueThreadId;         
	struct _node_s* next;
	struct _node_s* prev;
}Node;

typedef struct queue_t{
	Node* head;
	Node* tail;
	int count;
}queue_t;

typedef struct qArgPthread{
	queue_t* queue;
	int mThreadId;
	int data;
}qArgPthread;

void qInitQueue(queue_t* aQueue);

void add_Node(void* aArgPthread);

void delete_First_Node(void* aArgPthread);

//void node_Search(queue_t* queue, int number);

//int list_Size(queue_t* queue);

//void print_List(queue_t* queue);
#endif
