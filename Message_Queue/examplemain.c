#include "Make_Queue.h"

pthread_mutex_t gMutexForThread;
pthread_mutex_t gMutexForEnqDeq[THREAD_COUNT];

pthread_cond_t gCondition[THREAD_COUNT];

///////////*Flag Variable*////////
int gMainFlag;
int gFlagForThread[THREAD_COUNT];
int gFlagForEnqDeq[THREAD_COUNT];
//------------------------------//

int main(void){
	pthread_t sEnqueueThread[THREAD_COUNT]; 
	pthread_t sDequeueThread[THREAD_COUNT];

	qArgPthread *sArgEnqueueThread[THREAD_COUNT];
	qArgPthread *sArgDequeueThread[THREAD_COUNT];

	int menu = 0;
	int data,n;
	int number;
	int check;

	int sIsCreatedThread=0;  //pthread_create: Thread_create complete or not 
	
	int sIsJoinedThread=0; // pthread_join : Thread_join complete or not 

	int sReturnValue=0; // pthread_join : Thread -> join complete : return value

	int sCountThread=0; // Thread_number: need for for문
	int sDoubleThreadCount=0;
	sDoubleThreadCount = THREAD_COUNT*2;

	queue_t* sQueue = NULL;

	sQueue =(queue_t*)malloc(sizeof(queue_t));
	
	pthread_mutex_init(&gMutexForThread, NULL);

	for(sCountThread=0;sCountThread<THREAD_COUNT;sCountThread++)
	{
		pthread_mutex_init(&gMutexForEnqDeq[sCountThread],NULL);
		pthread_cond_init(&gCondition[sCountThread],NULL);

		/*flags*/ 
		gFlagForThread[sCountThread]=0;
		gFlagForEnqDeq[sCountThread]=0;
	}

	gMainFlag =0;
	
	qInitQueue(sQueue);

	pthread_mutex_lock(&gMutexForThread);

	/* Thread Create*/

	/* Add Node Thread*/

	for(sCountThread=0; sCountThread<THREAD_COUNT;sCountThread++)
	{
		sArgEnqueueThread[sCountThread]=(qArgPthread*)malloc(sizeof(qArgPthread));
		sArgEnqueueThread[sCountThread]->queue = sQueue;
		sArgEnqueueThread[sCountThread]->mThreadId=sCountThread;

		sIsCreatedThread = pthread_create(&sEnqueueThread[sCountThread],NULL,&add_Node,(void*)sArgEnqueueThread[sCountThread];)
		if(sIsCreated !=0)
		{
			printf("[ERROR-001] :: cannot create thread.\n");
		}
	}

	for(sCountThread=0; sCountThread <THREAD_COUNT; sCountThread++)
	{
		sArgDequeueThread[sCountThread]=(qArgPthread*)malloc(sizeof(qArgPthread));

		sArDequeueThread[sCountThread]->queue = sQueue;
		sArgDequeueThread[sCountThread]->mThreadId = sCountThread;

		sIsCreateThread = pthread_create(&sDequeueThread[sCountThread],NULL,&delete_First_Node,(void*)sArgDequeueThread[sCountThreadId]);
		
		if(sIsCreated !=0)
		{
			printf("[ERROR-001] :: cannot create thread.\n");
		}
	}

	while(gMainFlag<sDoubleThreadCount)
	{
		gMainFlag =0;
		for(sCountThread =0;sCountThread<THREAD_COUNT;sCountThread++)
		{
			gMainFlag +=gFlagForThread[sCountThread];
		}
		sleep(0);
	}

	pthread_mutex_unlock(&gMutexForThread);

	for(sCountThread=0;sCountThread<THREAD_COUNT; sCountThread++)
	{
		sIsJoinedThread = pthread_join(sEnqueueThread[sCountThread],(void**)&sReturnValue);
		if(sIsJoined !=0)
		{
			printf("[ERROR-002] :: cannot join enqueue thread.\n");
		}

		sIsJoinedThread = pthread_join(sDequeueThread[sCountThread],(void**)&sReturnValue);
		if(sIsJoined !=0)
		{
			printf("[ERROR-002] :: cannot join dequeue thread.\n");
		}

		printf("\n+------------------------------------------------+");
		printf("\n-------[SYSTEM] :: program is exited............\n");
		printf("+-------------------------------------------------+\n\n");
		return 0;
	}
/*
	void print_Menu(){
		printf("1.Add Node(Last position)\n");
		printf("2.First Node Delete\n");
		printf("3.Data Search\n");	
		printf("4.List Print\n");
		printf("5.Menu Print\n");
		printf("6.Quit\n");	
	}	
	print_Menu();
		
	while(menu!=6){
		printf("Select:");
		check = scanf("%d",&menu);
		while(getchar() !='\n');
		if(check==0) {
			printf("Only Insert Int data(1~6)\n\n");
			continue;
		}								
		switch(menu){
					
			case 1:
				printf("\nInsert data:");
				check=scanf("%d",&data);		
				while(getchar() !='\n');
				if(check==0){	
					printf("Invalid data.\n\n");
					continue;
				}
				add_Node(queue,data);
				break;
			
			case 2:
				printf("\nNode Delete\n");
				delete_First_Node(queue);
				break;
				
			case 3:
				printf("\nWhat data you want to search: ");
				check=scanf("%d",&number);
				while(getchar() != '\n');
				if(check==0){
					printf("Invalid data\n\n");
					continue;	
				}
				node_Search(queue,number);
				break;
			case 4:
				printf("\nAll List Print\n");
				print_List(queue);
				break;
			case 5:
				printf("\nMenu Print\n\n");
				print_Menu();
			case 6:
				break;	
			default:
					printf("\nFault Choice\n\n");
		}
		
	}
*/
}
