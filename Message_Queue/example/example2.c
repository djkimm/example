#include "example.h"

/* Enqueue와 Dequeue가 스레드를 통해 동시 진행 
 * Enqueue 스레드가 enqueue 이후 signal을 전송
 * Dequeue 스레드가 siganl을 받고 dequeue를 수행
 * 다시 Enqueue 스레드에게 작업을 마친 flag를 넘긴다.*/
// flag를 넘기는게 어떤건지 ?


/* MUTEX 객체 선언 */

pthread_mutex_t  gMutexForThread;
pthread_mutex_t  gMutexForEnqDeq[THREAD_COUNT];

/* Condition Variable 객체 선언*/

pthread_cond_t   gCondition[THREAD_COUNT];

/* flag라는데 뭔지 모르겠음 */

int gMainFlag;
int gFlagForThread[THREAD_COUNT];
int gFlagForEnqDeq[THREAD_COUNT];



int main()
{
	   
	pthread_t sEnqueueThread[THREAD_COUNT];  // Thread_count 만큼의Enqueue thread 생성  
	pthread_t sDequeueThread[THREAD_COUNT]; // Thread_count 만큼의 Dequeue thread 생성     
			    
	/* 구조체에서 생성한것 -> Thread에서 생성 시 인자로 넘겨주는 구조체*/

	qArgPthread * sArgEnqueueThread[THREAD_COUNT]; // Thread_count만큼 인자로 넘겨줄 구조체(Enqueue)
	qArgPthread * sArgDequeueThread[THREAD_COUNT];// Thread_count만큼 인자로 넘겨줄 구조체(Dequeue)

	qQueue* sQueue              = NULL; 

	int	sIsCreatedThread    = 0;         	
	int	sIsJoinedThread     = 0;          
	int	sReturnValue        = 0;          	
	int sCountThread        = 0; //Thread count(개수)          
	int sDoubleThreadCount  = 0;
											    
	sQueue=(qQueue *)malloc( sizeof( qQueue ));
	
	sDoubleThreadCount = THREAD_COUNT * 2; 

	/* 위에서 선언한 1번쨰 Mutex Initialize*/
	pthread_mutex_init( &gMutexForThread, NULL );

	for( sCountThread = 0; sCountThread < THREAD_COUNT; sCountThread++ ) 
	{	
		pthread_mutex_init( &gMutexForEnqDeq[sCountThread], NULL );								
		pthread_cond_init( &gCondition[sCountThread], NULL );
		gFlagForThread[sCountThread] = 0;		
		gFlagForEnqDeq[sCountThread] = 0;
	}
	gMainFlag = 0;  															    	
	qInitQueue( sQueue ); 


	pthread_mutex_lock( &gMutexForThread );

	for( sCountThread = 0; sCountThread < THREAD_COUNT; sCountThread++ )
	
	{
		sArgEnqueueThread[sCountThread] = (qArgPthread *)malloc( sizeof( qArgPthread ) );
	sArgEnqueueThread[sCountThread]->mQueue     = sQueue;
		sArgEnqueueThread[sCountThread]->mThreadId  = sCountThread;
		sIsCreatedThread = pthread_create( &sEnqueueThread[sCountThread],NULL,&qEnqueue,(void *)sArgEnqueueThread[sCountThread] );
		if( sIsCreatedThread != 0 )
		{
			printf("[ERROR-001] :: cannot create thread.\n");	
		}
	}



	for( sCountThread = 0; sCountThread < THREAD_COUNT; sCountThread++ )
	{
		sArgDequeueThread[sCountThread] = (qArgPthread *)malloc( sizeof( qArgPthread ) );
		sArgDequeueThread[sCountThread]->mQueue     = sQueue;
		sArgDequeueThread[sCountThread]->mThreadId  = sCountThread;
		sIsCreatedThread = pthread_create( &sDequeueThread[sCountThread],
		
				
				NULL,
				
				&qDequeue,
				
				(void *)sArgDequeueThread[sCountThread] );


		
		if( sIsCreatedThread != 0 )
		
		{
		
			printf("[ERROR-001] :: cannot create thread.\n");
			
		}
		
	}


	

	
	
	while( gMainFlag < sDoubleThreadCount )
	
	{
	
		gMainFlag = 0;
		
		
		for( sCountThread = 0; sCountThread < THREAD_COUNT; sCountThread++ )
		
		{
		
			
			gMainFlag += gFlagForThread[sCountThread];
			
		}
		
		sleep(0);
		
	
	}


	

	
	pthread_mutex_unlock( &gMutexForThread );
	


	



	
	
	for( sCountThread = 0; sCountThread < THREAD_COUNT; sCountThread++ )
	
	{
	


		
		sIsJoinedThread = pthread_join( sEnqueueThread[sCountThread],
		
				(void **)&sReturnValue );



		
		if( sIsJoinedThread != 0 )
		
		
		{
		
			
			printf("[ERROR-002] :: cannot join enqueue thread.\n");
			
		}



		


		

		
		sIsJoinedThread = pthread_join( sDequeueThread[sCountThread],
		
				(void **)&sReturnValue );



		
		if( sIsJoinedThread != 0 )
		
		
		{
		
			printf("[ERROR-002] :: cannot join dequeue thread.\n");
			
		}
		

		
	
	}


	
	printf("\n+-----------------------------------+");
	
	printf("\n| [SYSTEM] :: program is Exited.... |\n");
	
	
	printf("+-----------------------------------+\n\n");
	

	
	
	return 0;
}







void qInitQueue( qQueue * aQueue )
{
	
	aQueue->mFrontNode  = NULL;	
	aQueue->mRearNode   = NULL;
	aQueue->mCount      = 0;
	
	printf("\n+-----------------------------------+");
	
	printf("\n| [SYSTEM] :: queue is Initialized. |\n");
	
	printf("+-----------------------------------+\n\n");
}




void  * qEnqueue( void * aArgPthread )
{
	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	qNode       * sNewNode      = NULL;
	
	int           sInputData    = 0;
	int           sRepeatCount  = 0;
	int           sThreadId     = 0;

	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue      = sArgPthread->mQueue;
	sThreadId   = sArgPthread->mThreadId;

	printf("[#%3d][ENQUEUE_THREAD] is ready.\n", sThreadId );
	gFlagForThread[sThreadId] += 1;
	while( sRepeatCount < DATA_MAX_COUNT )
	{

		while( gFlagForEnqDeq[sThreadId] == 0 )
		{
			sleep(0);
		}
		pthread_mutex_lock( &gMutexForThread );
		sNewNode = (qNode *)malloc( sizeof( qNode ) );
		if( sQueue->mCount >= THREAD_COUNT /* sMaxBuff */ )
		{
			
		}
		else
		{
			sInputData                  = rand() % 90 + 10; 
			sNewNode->mData             = sInputData;
			sNewNode->mNextNode         = NULL;
			sNewNode->mEnqueueThreadId  = sThreadId;
			printf("[#%3d][ENQUEUE_THREAD] Enqueue DATA : %d( %d/%d ) QUEUE STATUS : [ %d/%d ]\n",
					sThreadId,
					sInputData,
					sRepeatCount+1,
					DATA_MAX_COUNT,
					sQueue->mCount+1,
					THREAD_COUNT /* sMaxBuff */ );
			if( sQueue->mCount == 0 )
			{
				sQueue->mFrontNode   = sNewNode;
				sNewNode->mPrevNode  = NULL;
			}
			else
			{
				sQueue->mRearNode->mNextNode  = sNewNode;
				sNewNode->mPrevNode           = sQueue->mRearNode;
			}
			sQueue->mRearNode  = sNewNode;
			sQueue->mCount++;
			sRepeatCount++;
		}
		pthread_mutex_unlock( &gMutexForThread );
		pthread_mutex_lock( &gMutexForEnqDeq[sThreadId] );
		gFlagForEnqDeq[sThreadId] = 0; 
		pthread_cond_signal( &gCondition[sThreadId] );
		pthread_mutex_unlock( &gMutexForEnqDeq[sThreadId] );
		usleep(50);
	}
}

void  * qDequeue( void * aArgPthread )
{
	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	
	int       sOutputData       = 0;
	int       sRepeatCount      = 0;
	int       sThreadId         = 0;
	int       sEnqueueThreadId  = 0;
	
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue     = sArgPthread->mQueue;
	sThreadId  = sArgPthread->mThreadId;
	
	printf("[#%3d][DEQUEUE_THREAD] is ready.\n", sThreadId );
	gFlagForThread[sThreadId] += 1;
	while( sRepeatCount < DATA_MAX_COUNT )
	{
		pthread_mutex_lock( &gMutexForEnqDeq[sThreadId] );
		gFlagForEnqDeq[sThreadId] = 1;
		pthread_cond_wait( &gCondition[sThreadId], &gMutexForEnqDeq[sThreadId] );
		pthread_mutex_unlock( &gMutexForEnqDeq[sThreadId] );
		pthread_mutex_lock( &gMutexForThread );
		
		if( sQueue->mCount == 0 )
		{
		}
		else
		{
			sOutputData       = sQueue->mFrontNode->mData;
			sEnqueueThreadId  = sQueue->mFrontNode->mEnqueueThreadId;
			printf("[#%3d][DEQUEUE_THREAD] Dequeue DATA : %d( %d/%d ) QUEUE STATUS : [ %d/%d ] data by [#%3d][ENQUEUE_THREAD]\n",sThreadId,sOutputData,sRepeatCount+1,DATA_MAX_COUNT,sQueue->mCount-1,THREAD_COUNT /* sMaxBuff */,
sEnqueueThreadId );

			if( sQueue->mCount == 1 )
			{
				free( sQueue->mFrontNode );
				sQueue->mFrontNode  = NULL;
				sQueue->mRearNode   = NULL;
				sQueue->mCount      = 0;
			}
			else
			{
				sQueue->mFrontNode = sQueue->mFrontNode->mNextNode;
				free( sQueue->mFrontNode->mPrevNode );
				sQueue->mFrontNode->mPrevNode = NULL;
				sQueue->mCount--;
			}
			sRepeatCount++;
		}
		pthread_mutex_unlock( &gMutexForThread );
	}
}

/* repeatecount, mcount what role 
 * enque -> insert_node 수정 
 * deque -> delete_node 수정
 * thread_id -> code analysis 후 지울거 지우기
 */
