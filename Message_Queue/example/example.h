#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>


#define THREAD_COUNT 1
#define DATA_MAX_COUNT 20

typedef struct qNode
{
	int mData;
	int mEnqueueThreadId;

	struct qNode* mPrevNode;
	struct qNode* mNextNode;

}qNode;

typedef struct qQueue
{
	qNode* mFrontNode;
	qNode* mRearNode;
	int mCount;
}qQueue;

typedef struct qArgPthread
{
	qQueue* mQueue;
	int mThreadId;
}qArgPthread;

void qInitQueue(qQueue* aQueue);

void* qEnqueue(void* aArgPthread);

void* qDequeue(void* aArgPthread);

