#include "example.h"

//=============================================================
//List 초기화 함수
//============================================================
void qInitQueue(queue_t* aQueue)
{
	aqueue->head = NULL;
	aqueue->tail = NULL;
	aqueue->count = 0;

	printf("----------------------------------\n");
	printf("Queue Start\n");
	printf("----------------------------------\n");
	//return queue;
}
//==================노드 추가 함수===========================
//================== 2. Tail에추가==========================
void* add_Node(void* aArgPthread)
{
	qArgPthread* sArgPthread =NULL;
	queue_t* sQueue = NULL;
	Node* newNode = NULL;

	int sInputData =0;
	int sRepeatCount=0;
	int sThreadId=0;

	sArgPthread =(qArgPthread*)aArgPthread;
	
	sQueue = sArgPthread -> queue;
	sThreadId =sArgPthread->mThreadId;

	printf("[#%d][INSERT DATA_THREAD] is ready.\n",sThreadId);
	gFlagForThreadId[sThreadId]+=1;
	
	while(sRepeatCount<MAX_SIZE)
	{
		while(gFlagForEnqDeq[sThreadId] ==0)
		{
			sleep(0);
		}

		pthread_mutex_lock(&gMutexForThread);

		newNode =(Node*)malloc(sizeof(Node));

		if(sQueue->count >= THREAD_COUNT)
		{
		}
		else
		{
			sInputData=rand() %90+10; /*임의의 데이터이므로 바꿀 수 있는지 확인*/
			newNode->data = sInputData;
			newNode->next = NULL;
			newNode->mEnqueueThreadId = sThreadId;

			printf("[#%3d][ENQUEUE_THREAD] Enqueue DATA : %d( %d/%d ) QUEUE STATUS : [ %d/%d ]\n",sThreadId,sInputData,
sRepeatCount+1,MAX_SIZE,sQueue->count+1,THREAD_COUNT /* sMaxBuff */ );
			         
			if( sQueue->count == 0 )		
			{
				sQueue->head= newNode;	
				newNode->prev= NULL;
			}	
			else
			{
				sQueue->tail->next=newNode;
				
				newNode->prev=sQueue->tail;
			}
			sQueue->tail=newNode;
			sQueue->count++;
			sRepeatCount++;
		}

		pthread_mutex_unlock(&gMutexForThread);
		pthread_mutex_lock(&gMutexForEnqDeq[sThreadId]);
		gFlagForEnqDeq[sThreadId]=0;
		pthread_cond_signal(&gCondition[sThreadId]);
		pthread_mutex_unlock(&gMutexForEnqDeq[sThreadId]);

		usleep(50);
	}
}
//===============1. Head 노드 삭제 함수===============


void* delete_First_Node(void* aArgPthread)
{
	
	qArgPthread* sArgPthread =NULL;
	queue_t sQueue = NULL;

	int sOutputData =0;
	int sRepeatCount=0;
	int sThreadId=0;
	int sEnqueueThreadId=0;

	sArgPthread =(qArgPthread*)aArgPthread;
	
	sQueue = sArgPthread -> queue;
	sThreadId =sArgPthread->mThreadId;

	printf("[#%d][DELETE DATA_THREAD] is ready.\n",sThreadId);
	gFlagForThreadId[sThreadId]+=1;
	
	while(sRepeatCount<MAX_SIZE)
	{
		pthread_mutex_lock(&gMutexForEnqDeq[sThreadId]);

		gFlagForEnqDeq[sThreadId] =1;

		pthread_cond_wait( &gCondition[sThreadId], &gMutexForEnqDeq[sThreadId] );

		pthread_mutex_unlock( &gMutexForEnqDeq[sThreadId] );

		pthread_mutex_lock( &gMutexForThread );


		if(sQueue->count ==0)
		{
		}
		else
		{
			sOuputputData = sQueue->head->data;

			sEnqueueThreadId = sQueue->head->mEnqueueThreadId;

			printf("[#%3d][DEQUEUE_THREAD] DEqueue DATA : %d( %d/%d ) QUEUE STATUS : [ %d/%d ]\n",sThreadId,sOutputData,
sRepeatCount+1,MAX_SIZE,sQueue->count-1,THREAD_COUNT,sEnqueueThreadId);
			         
			if( sQueue->count == 1 )		
			{
				free(sQueue->head);
				sQueue ->head=NULL;
				sQueue->tail=NULL;
				sQueue->count=0;
			}	
			else
			{
				sQueue->head = sQueue->head->next;
				free(sQueue->head->prev);
				sQueue->head->prev=NULL;
				SQueue->count--;
			}
			sRepeatCount++;
		}
		pthread_mutex_unlock( &gMutexForThread );
		usleep(50);
	}
}
