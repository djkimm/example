#ifndef __MAKE_QU_EUE_H__
#define __MAKE_QU_EUE_H__

#define MAX_SIZE 20

#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>

typedef struct _node_s{
	int data;
	struct _node_s* next;
	struct _node_s* prev;
}Node;

typedef struct queue_t{
	Node* head;
	Node* tail;
	int count;
	pthread_mutex_t mutex;
	        
	pthread_cond_t  c_cond;
			       
	pthread_cond_t  p_cond;
}queue_t;

queue_t que={

	.p_cond=PTHREAD_COND_INITIALIZER,
	
	.c_cond=PTHREAD_COND_INITIALIZER,
	
	.mutex=PTHREAD_MUTEX_INITIALIZER

};

queue_t *Init();

void add_Node(queue_t* queue, int data);

void delete_First_Node(queue_t* queue);

void node_Search(queue_t* queue, int number);

int list_Size(queue_t* queue);

void print_List(queue_t* queue);
#endif
