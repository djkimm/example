#include "example2.h"

int main(void){

	pthread_t sEnqueueThread[THREAD_COUNT]; /*THREAD_COUNT 만큼의 ENQUEUE Thread 선언*/
	pthread_t sDequeueThread[DEL_THREAD_COUNT]; /*THREAD_COUNT 만큼의 DEQUEUE Thread 선언---> 변경해야할 사항 */

	qArgPthread* sArgEnqueueThread[THREAD_COUNT]; /*THREAD_COUNT만큼의 인자로 넘겨줄 구조체 */
	qArgPthread* sArgDequeueThread[DEL_THREAD_COUNT]; /*위와 같음 */

	qQueue *sQueue = NULL;

	int sIsCreatedThread=0;
	int sIsJoinedThread=0;
	int sReturnValue=0;
	int sCountThread=0;
	int sDelCountThread=0;
	int sDoubleThreadCount=0;
	int sDelDoubleThreadCount=0;

	sQueue =(qQueue*)malloc(sizeof(qQueue));

	sDoubleThreadCount = THREAD_COUNT *2;
	sDelDoubleThreadCount = DEL_THREAD_COUNT*2;

	pthread_mutex_init(&gMutexForThread,NULL);
	
	for(sCountThread=0; sCountThread <THREAD_COUNT; sCountThread++)
	{
		pthread_mutex_init(&gMutexForEnqDeq[sCountThread],NULL);
		pthread_cond_init(&gCondition[sCountThread],NULL);
		gFlagForThread[sCountThread]=0;
		gFlagForEnqDeq[sCountThread]=0;
	}

	for(sDelCountThread=0; sDelCountThread<DEL_THREAD_COUNT;sDelCountThread++)
	{
		gDelFlagForThread[sDelCountThread]=0;
	}

	gMainFlag =0;
	gDelMainFlag =0;

	qInitQueue(sQueue); /*sQueue Initialize*/

	pthread_mutex_lock(&gMutexForThread);

	for(sCountThread =0; sCountThread<THREAD_COUNT;sCountThread++)
	{
		sArgEnqueueThread[sCountThread] =(qArgPthread*)malloc(sizeof(qArgPthread));

		sArgEnqueueThread[sCountThread]->mQueue = sQueue;
		sArgEnqueueThread[sCountThread]->mThreadId = sCountThread;

		sIsCreatedThread = pthread_create(&sEnqueueThread[sCountThread],NULL,&add_Node,(void*)sArgEnqueueThread[sCountThread]);
		if(sIsCreatedThread !=0)
		{
			printf("[ERROR] :: cannot create thread.\n");
		}
	}

	for(sDelCountThread =0; sDelCountThread<DEL_THREAD_COUNT;sDelCountThread++)
	{
		sArgDequeueThread[sDelCountThread] =(qArgPthread*)malloc(sizeof(qArgPthread));

		sArgDequeueThread[sDelCountThread]->mQueue = sQueue;
		sArgDequeueThread[sDelCountThread]->mThreadId = sDelCountThread;

		sIsCreatedThread = pthread_create(&sDequeueThread[sDelCountThread],NULL,&delete_First_Node,(void*)sArgDequeueThread[sDelCountThread]);
		if(sIsCreatedThread !=0)
		{
			printf("[ERROR] :: cannot create thread.\n");
		}
	}


	while(gMainFlag < sDoubleThreadCount)
	{
		gMainFlag =0;
		for(sCountThread=0; sCountThread < THREAD_COUNT; sCountThread++)
		{
			gMainFlag += gFlagForThread[sCountThread];
		}
		sleep(0);
	}

	while(gDelMainFlag < sDelDoubleThreadCount)
	{
		gDelMainFlag=0;
		for(sDelCountThread=0; sDelCountThread < DEL_THREAD_COUNT;sDelCountThread++)
		{
			gDelMainFlag += gDelFlagForThread[sDelCountThread];
		}
		sleep(0);
	}


	pthread_mutex_unlock(&gMutexForThread);

	for(sCountThread=0; sCountThread < THREAD_COUNT; sCountThread++)
	{
		sIsJoinedThread = pthread_join(sEnqueueThread[sCountThread],(void**)&sReturnValue);

		if(sIsJoinedThread !=0)
		{
			printf("[ERROR] :: cannot join enqueue Thread.\n");
		}
	}
	for(sDelCountThread=0; sDelCountThread<DEL_THREAD_COUNT; sDelCountThread++)
	{
		sIsJoinedThread = pthread_join(sDequeueThread[sDelCountThread],(void**)&sReturnValue);

		if(sIsJoinedThread !=0)
		{
			printf("[ERROR] :: cannot join dequeue Thread.\n");
		}
	}

	printf("\n------------------------------------");
	printf("\n|[SYSTEM] :: PROGRAM IS EXITED....|\n");
	printf("------------------------------------\n\n");

	return 0;
}

//=============================================================
//List 초기화 함수

void qInitQueue(qQueue* aQueue)
{  
	aQueue->head = NULL;
	aQueue->tail = NULL;
	aQueue->count = 0;

	printf("\n-------------------------------------");
	printf("\n|[SYSTEM] :: Queue is Initialized.|\n");
	printf("-------------------------------------\n\n");
}

//==================노드 추가 함수===========================
//================== 2. Tail에추가==========================
void *add_Node(void* aArgPthread)
{
	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	qNode       * newNode      = NULL;

	int 	      check,data;
	int           sInputData    = 0;
	int           sRepeatCount  = 0;
	int           sThreadId     = 0;
	
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue      = sArgPthread->mQueue;
	sThreadId   = sArgPthread->mThreadId;

	printf("[#%3d][ENQUEUE_THREAD] is ready.\n",sThreadId);
	gFlagForThread[sThreadId]+=1;

	while(sRepeatCount<DATA_MAX_COUNT)
	{
		while(gFlagForEnqDeq[sThreadId] ==0)
		{
			sleep(0);
		}
		pthread_mutex_lock(&gMutexForThread);

		qNode* newNode = (qNode*)malloc(sizeof(qNode));

		if(sQueue->count >=THREAD_COUNT)
		{
			printf("\nQUEUE IS FULL");
		}
		else
		{
			sInputData = rand() %90+10;; // DATA는 scanf로 입력받아야함 ..
			newNode->data = sInputData;
			newNode->next = NULL;
			newNode->mEnqueueThreadId=sThreadId;
							
			printf("[#%3d][ENQUEUE_THREAD] Enqueue DATA : %d( %d/%d ) QUEUE STATUS : [ %d/%d ]\n",
					sThreadId,
					sInputData,
					sRepeatCount+1,
					DATA_MAX_COUNT,
					sQueue->count+1,
					THREAD_COUNT /* sMaxBuff */ );
			           
			if( sQueue->count == 0 )
			{
				sQueue->head = newNode;
				sQueue->tail =newNode;
				newNode->next =NULL;
				newNode->prev  = NULL;
			}

			else
			{
				sQueue->tail->next=newNode;
				newNode->next=NULL;
				newNode->prev=sQueue->tail;
			}
			sQueue->tail = newNode;
			sQueue->count++;
			sRepeatCount++;
		}

		pthread_mutex_unlock(&gMutexForThread);
		pthread_mutex_lock(&gMutexForEnqDeq[sThreadId]);
		gFlagForEnqDeq[sThreadId] =0;
		pthread_cond_signal(&gCondition[sThreadId]);
		pthread_mutex_unlock(&gMutexForEnqDeq[sThreadId]);
		usleep(50);
	}
}

//===============1. Head 노드 삭제 함수===============

void* delete_First_Node(void* aArgPthread)
{
	qArgPthread* sArgPthread = NULL;
	qQueue* sQueue = NULL;

	int sOutputData =0;
	int sRepeatCount =0;
	int sThreadId=0;
	int sEnqueueThreadId =0;

	sArgPthread =(qArgPthread*)aArgPthread;

	sQueue = sArgPthread->mQueue;
	sThreadId =sArgPthread->mThreadId;

	printf("[$%3d][DEQUEUE_THREAD] is ready.\n",sThreadId);
	gDelFlagForThread[sThreadId]+=1;

	while( sRepeatCount < DATA_MAX_COUNT )
		   
	{
		pthread_mutex_lock( &gMutexForEnqDeq[sThreadId] );
		gFlagForEnqDeq[sThreadId] = 1;
		pthread_cond_wait( &gCondition[sThreadId], &gMutexForEnqDeq[sThreadId] );
		pthread_mutex_unlock( &gMutexForEnqDeq[sThreadId] );
		pthread_mutex_lock( &gMutexForThread );
		if(sQueue->count ==0)
		{
			printf("\n QUEUE IS EMPTY.");
		}
		else
		{
			sOutputData = sQueue->head->data;
			sEnqueueThreadId = sQueue->head->mEnqueueThreadId;
			  printf("[#%3d][DEQUEUE_THREAD] Dequeue DATA : %d( %d/%d ) QUEUE STATUS : [ %d/%d ] data by [#%3d][ENQUEUE_THREAD]\n",
					  sThreadId,
					  sOutputData,
					  sRepeatCount+1,
					  DATA_MAX_COUNT,
					  sQueue->count-1,
					  DEL_THREAD_COUNT /* sMaxBuff */,
					  sEnqueueThreadId );
			  
			  if( sQueue->count == 1 )
			  {
				  free( sQueue->head );
				  sQueue->head  = NULL;
				  sQueue->tail   = NULL;
				  sQueue->count      = 0;
			  }
			  else
			  {
				  sQueue->tail = sQueue->tail->next;
				  free( sQueue->head->prev);
				  sQueue->head->prev= NULL;
				  sQueue->count--;
			  }
			  sRepeatCount++;
		}

		pthread_mutex_unlock(&gMutexForThread);
		usleep(50);
	}
}


