#ifndef __EXAMPLE2_H__
#define __EXAMPLE2_H__

#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>

#define THREAD_COUNT 2
#define DEL_THREAD_COUNT  3 
#define DATA_MAX_COUNT 4

typedef struct _node_s{
	int data;
	int mEnqueueThreadId;
	struct _node_s* next;
	struct _node_s* prev;
}qNode;

typedef struct queue_t{
	qNode* head;
	qNode* tail;
	int count;
}qQueue;

typedef struct qArgPthread
{
	qQueue *mQueue;
	int mThreadId;
}qArgPthread;

/*Initialize Queue*/
void qInitQueue(qQueue* aQueue);

/*Enqueue*/
void* add_Node(void* aArgPthread);

/*Dequeue*/
void* delete_First_Node(void* aArgPthread);

//void node_Search(queue_t* queue, int number);

//int list_Size(queue_t* queue);

//void print_List(queue_t* queue);

pthread_mutex_t gMutexForThread;
pthread_mutex_t gMutexForEnqDeq[THREAD_COUNT];
pthread_cond_t gCondition[THREAD_COUNT];

int gMainFlag;
int gDelMainFlag;
int gFlagForThread[THREAD_COUNT];
int gDelFlagForThread[DEL_THREAD_COUNT];
int gFlagForEnqDeq[THREAD_COUNT];

#endif
