#include "Message_Queue.h"
queue_t* queue =NULL;
//=============================================================
//List 초기화 함수
//============================================================
queue_t* Init()
{  
	queue_t* queue = (queue_t*)malloc(sizeof(queue_t));

	if(queue== NULL){
		printf("Malloc Fail\n");
		return NULL;
	}
	queue->head = NULL;
	queue->tail = NULL;
	queue->count = 0;

	printf("----------------------------------\n");
	printf("Queue Start\n");
	printf("----------------------------------\n");
	return queue;
}

int is_empty(){
	return queue->count==0;
}

int is_full(){
	return queue->count==MAX_SIZE-1;
}

//==================노드 추가 함수===========================
//================== 2. Tail에추가==========================
void add_Node(int data)
{
	Node* newNode = (Node*)malloc(sizeof(Node));

	newNode->data =data;


	if(queue->tail==NULL){	
		queue->head = queue->tail = newNode;
		newNode->next=NULL;
		newNode->prev=NULL;

		printf(" -> data added(First)......(%d)\n", newNode->data);
		printf("\n");
	}
	else{
		if(queue->count>MAX_SIZE-1)
		{
			printf("LIST IS FULL\n\n");
			return;
		}
		else{
		queue->tail->next=newNode;
		newNode->next=NULL;
		newNode->prev=queue->tail;
		queue->tail=newNode;

		printf(" -> data added(Last)......(%d)\n", newNode -> data);
		printf("\n");
		}
	}
		queue->count++;
		
}

//===============1. Head 노드 삭제 함수===============

void delete_First_Node(queue_t* queue)
{
	Node* cur;
	Node* tmp;

	tmp = queue->head;

	if(tmp == NULL){
		printf("List is empty\n\n");
		return;
	}
	
	if(queue->count ==1)
	{		
		printf(" -> data deleted(First)......(%d)\n\n", tmp->data);
		queue->head =NULL;
		queue->tail =NULL;
		free(tmp);
		queue->count--;
	}

	else{
		cur = queue->head->next;
		cur->prev = NULL;// list -> head; //next 지움
		queue->head = cur;

		printf(" -> data deleted(First)......(%d)\n\n", tmp->data);
		free(tmp);
		queue->count--;
	}
}
//=========== 데이터 탐색 함수=================//

void node_Search(queue_t* queue, int number)
{
	Node* cur = queue->head;
	int node_Number = queue->count;

	if(queue->head ==NULL)
	{
		printf("\n저장된 데이터가 없습니다.\n");
	}
	else
	{
		for(int i=0;i<node_Number;i++)
		{
			if(cur->data ==number)
			{
				printf("저장된 %d의 데이터를 출력하였습니다.\n\n",number);
			}
		cur = cur->next;
		if(cur == NULL){
			printf("더 이상 찾는 값이 없습니다.\n\n");
			return;
		}
		}
	}
}

int list_Size(queue_t* queue){

	return queue->count;
}

void print_List(queue_t* queue){ 
	Node *cur;
				               
	cur = queue->head;
	
	if(cur==NULL){

		puts("List is empty.\n");
		
	}
				                
	else{
		
		int n = 1;

		while(n <= queue->count){

			printf("List[%d] = %d.\n", n, cur->data);
			
			cur = cur->next; 
			
			n++;
			
		}
	}
	printf("현재 데이터의 수:%d\n",list_Size(queue));
	printf("\n");

}
