#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define THREAD_COUNT 3
#define DELETE_THREAD_COUNT 1
#define DATA_MAX_COUNT 1 

typedef struct qNode
{
	int data;      
	int mEnqueueThreadId;  
	struct qNode  * prev; 
	struct qNode  * next;
} qNode;

typedef struct qQueue
{
	qNode* head;    
	qNode* tail;    
	int   count;  
} qQueue;

typedef struct qArgPthread
{
	qQueue* mQueue;      
	int   mThreadId;  
} qArgPthread;

void  qInitQueue(qQueue * aQueue);
void *qEnqueue(void* aArgPthread);
void *qDequeue(void* aArgPthread );

pthread_mutex_t gMutexForThread;
pthread_mutex_t gMutexForEnqDeq[THREAD_COUNT];
pthread_mutex_t gMutexForDelDeq[DELETE_THREAD_COUNT];
/*------------------------------------------------*/
/*-----------------------------------------------
 * -------------------------------------------
 *  ----------------------------------------------*/
pthread_cond_t d_cond;
pthread_cond_t a_cond;
int main()
{
	pthread_t sEnqueueThread[THREAD_COUNT];    
	pthread_t sDequeueThread[DELETE_THREAD_COUNT];
	qArgPthread* sArgEnqueueThread[THREAD_COUNT];   
	qArgPthread* sArgDequeueThread[DELETE_THREAD_COUNT];
	
	qQueue* sQueue=NULL;
	
	int sIsCreatedThread=0;    
	int sIsJoinedThread=0;   
	int sReturnValue=0;  
	int sCountThread=0;       
	
	sQueue=(qQueue*)malloc(sizeof(qQueue));
	pthread_mutex_init(&gMutexForThread,NULL);
	pthread_cond_init(&d_cond,NULL);
	pthread_cond_init(&a_cond,NULL);
	for(sCountThread=0;sCountThread<THREAD_COUNT;sCountThread++)
	{
		pthread_mutex_init(&gMutexForEnqDeq[sCountThread],NULL);
	}
	for(sCountThread=0;sCountThread<DELETE_THREAD_COUNT;sCountThread++)
	{
		pthread_mutex_init(&gMutexForDelDeq[sCountThread],NULL);
	}
	qInitQueue(sQueue);     

	pthread_mutex_lock(&gMutexForThread);

	/* Enqueue Thread Create */
	for( sCountThread = 0; sCountThread < THREAD_COUNT; sCountThread++ )
	{
		sArgEnqueueThread[sCountThread] = (qArgPthread *)malloc(sizeof(qArgPthread));
		sArgEnqueueThread[sCountThread]->mQueue=sQueue;
		sArgEnqueueThread[sCountThread]->mThreadId=sCountThread;
		
		sIsCreatedThread = pthread_create(&sEnqueueThread[sCountThread],
				NULL,
				&qEnqueue,
				(void *)sArgEnqueueThread[sCountThread]);
		
		if( sIsCreatedThread != 0 )
		{
			printf("[ERROR-001] :: cannot create Addthread.\n");
		}
	}
																		      
	/* Dequeue Thread Create*/
	for(sCountThread=0; sCountThread<DELETE_THREAD_COUNT;sCountThread++)
	{
		sArgDequeueThread[sCountThread] = (qArgPthread *)malloc(sizeof(qArgPthread));
		sArgDequeueThread[sCountThread]->mQueue=sQueue;
		sArgDequeueThread[sCountThread]->mThreadId=sCountThread;
		
		sIsCreatedThread = pthread_create(&sDequeueThread[sCountThread],
				NULL,
				&qDequeue,
				(void *)sArgDequeueThread[sCountThread]);
		if( sIsCreatedThread != 0 )
		{
			printf("[ERROR-001] :: cannot create Delete thread.\n");
		}
	}
	pthread_mutex_unlock( &gMutexForThread );
   
	/* Thread Join*/
	for(sCountThread=0;sCountThread<THREAD_COUNT;sCountThread++)
	{
		/* Enqueue 스레드 */
		sIsJoinedThread = pthread_join(sEnqueueThread[sCountThread],
				0);
		if(sIsJoinedThread != 0)
		{
			printf("[ERROR-002] :: cannot join enqueue thread.\n");
		}
	}
	for(sCountThread = 0;sCountThread<DELETE_THREAD_COUNT;sCountThread++)  
	{ 
		/* Dequeue 스레드 */
		sIsJoinedThread = pthread_join(sDequeueThread[sCountThread],
				0);
		
		if( sIsJoinedThread != 0 )
		{
			printf("[ERROR-002] :: cannot join dequeue thread.\n");
		}
	}
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: program is Exited.... |\n");
	printf("+-----------------------------------+\n\n");
	return 0;
}

/* Initialize Queue */
void qInitQueue(qQueue* aQueue)
{
	aQueue->head  = NULL;
	aQueue->tail  = NULL;
	aQueue->count = 0;
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: queue is Initialized. |\n");
	printf("+-----------------------------------+\n\n");
}

/* Enqueue */
void  * qEnqueue( void * aArgPthread )
{
	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	qNode       * newNode      = NULL;
	
	int           sInputData    = 0;
	int           sRepeatCount  = 0;
	int           sThreadId     = 0;
	
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue      = sArgPthread->mQueue;
	sThreadId   = sArgPthread->mThreadId;
	
	printf("[#%3d][ENQUEUE_THREAD] is ready.\n", sThreadId );
	
	while( sRepeatCount < DATA_MAX_COUNT )
	{
		pthread_mutex_lock(&gMutexForEnqDeq[sThreadId]);
		newNode = (qNode *)malloc( sizeof( qNode ) );
		if(sQueue->count >= THREAD_COUNT)
		{
			pthread_cond_wait(&d_cond,&gMutexForEnqDeq[sThreadId]);
		}
		else
		{
			sInputData=rand()% 90+10;
			newNode->data = sInputData;
			newNode->next = NULL;
			newNode->mEnqueueThreadId = sThreadId;
			printf("[#%3d][ENQUEUE_THREAD] Enqueue DATA : %d QUEUE STATUS : [ %d/%d ]\n",
					sThreadId,
					sInputData,
					sQueue->count+1,
					THREAD_COUNT);
			if(sQueue->count==0)
			{
				sQueue->head   = newNode;
				sQueue->tail = newNode;
				newNode->next = NULL;
				newNode->prev=NULL;
			}
			else
			{
				sQueue->tail->next = newNode;
				newNode->next=NULL;
				newNode->prev = sQueue->tail;
	
			}
			sQueue->tail  = newNode;
			sQueue->count++;
			sRepeatCount++;
		}
		pthread_cond_signal(&a_cond);
		/* Enqueue Mutex Unlock */
		pthread_mutex_unlock( &gMutexForEnqDeq[sThreadId] );
	}
}

/* Dequeue */
void* qDequeue(void* aArgPthread)
{
	qArgPthread *sArgPthread = NULL;
	qQueue *sQueue=NULL;
	int sOutputData=0;
	int sRepeatCount=0;
	int sThreadId=0;
	int sEnqueueThreadId=0;
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue = sArgPthread->mQueue;
	sThreadId = sArgPthread->mThreadId;
	
	printf("[#%3d][DEQUEUE_THREAD] is ready.\n", sThreadId );
	
	while( sRepeatCount < DATA_MAX_COUNT*THREAD_COUNT )
	{
		pthread_mutex_lock(&gMutexForDelDeq[sThreadId]);
		if(sQueue->count == 0 )
		{
			pthread_cond_wait(&a_cond,&gMutexForDelDeq[sThreadId]);
		}
		else
		{
			sOutputData = sQueue->head->data;
			sEnqueueThreadId  = sQueue->head->mEnqueueThreadId;
			printf("[#%3d][DEQUEUE_THREAD] Dequeue DATA : %d QUEUE STATUS : [ %d/%d ] data by [#%3d][ENQUEUE_THREAD]\n",
					sThreadId,
					sOutputData,
					sQueue->count-1,
					THREAD_COUNT,
					sEnqueueThreadId);
			if( sQueue->count == 1)
			{
				free(sQueue->head);
				sQueue->head = NULL;
				sQueue->tail = NULL;
				sQueue->count = 0;
			}
			else
			{
				sQueue->head = sQueue->head->next;
				free(sQueue->head->prev);
				sQueue->head->prev = NULL;
				sQueue->count--;
			}
			sRepeatCount++;
		}
		pthread_cond_signal(&d_cond);
		pthread_mutex_unlock( &gMutexForDelDeq[sThreadId]);
	}
}
