#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define THREAD_COUNT 2
#define DELETE_THREAD_COUNT 2 
#define DATA_MAX_COUNT  4

/* 큐를 이루는 노드 구조체 */
typedef struct qNode
{
	int data;            /* Data */
	int mEnqueueThreadId;  /* 데이터를 준 스레드 ID */
	struct qNode  * prev;         /* 이전 Node */
	struct qNode  * next;         /* 다음 Node */
} qNode;

/* 큐 구조체 */
typedef struct qQueue
{
	qNode* head;      /* Dequeue */
	qNode* tail;       /* Enqueue */    
	int   count;          /* 노드 갯수 */
} qQueue;

/* 스레드 생성 시 인자로 넘겨주는 구조체 */
typedef struct qArgPthread
{
	qQueue* mQueue;         /* Queue */
	int   mThreadId;      /* 스레드 ID */
} qArgPthread;

/* Initialize Queue */
void  qInitQueue(qQueue * aQueue);
/* Enqueue */
void *qEnqueue(void* aArgPthread);
/* Dequeue */
void *qDequeue(void* aArgPthread );

/* Global Mutex & Condition Variable */
pthread_mutex_t gMutexForThread;
pthread_mutex_t gMutexForEnqDeq[THREAD_COUNT];
pthread_cond_t gCondition[THREAD_COUNT]; 
 
/* Flag Variables */
int  gMainFlag;
int  gFlagForThread[THREAD_COUNT];
int  gDelFlagForThread[DELETE_THREAD_COUNT];
int  gFlagForEnqDeq[THREAD_COUNT];


int main()
{
	pthread_t      sEnqueueThread[THREAD_COUNT];      /* THREAD_COUNT 만큼의 Enqueue 스레드 선언 */
	pthread_t      sDequeueThread[DELETE_THREAD_COUNT];      /* THREAD_COUNT 만큼의 Dequeue 스레드 선언 */
	qArgPthread  * sArgEnqueueThread[THREAD_COUNT];   /* THREAD_COUNT 만큼의 인자로 넘겨줄 구조체 */
	qArgPthread  * sArgDequeueThread[DELETE_THREAD_COUNT];   /* THREAD_COUNT 만큼의 인자로 넘겨줄 구조체 */
	
	qQueue       * sQueue              = NULL;
	
	int sIsCreatedThread    = 0;           /* 스레드가 Create되었는지 여부를 알기 위한 변수 */
	int sIsJoinedThread     = 0;           /* 스레드가 Join되었는지 여부를 알기 위한 변수 */
	int sReturnValue        = 0;           /* 스레드가 Join 수행 시 반환하는 값 */
	int sCountThread     = 0;           /* 스레드 개수, 각종 for문의 정수형 변수*/    
	int sDoubleThreadCount = 0;
	
	sQueue=(qQueue*)malloc( sizeof(qQueue));
	sDoubleThreadCount = THREAD_COUNT * 2;            /* THREAD_COUNT의 두배 */      
	pthread_mutex_init( &gMutexForThread, NULL );
	
	for( sCountThread = 0; sCountThread < THREAD_COUNT; sCountThread++ )
	{
		/* Mutex */        
		pthread_mutex_init( &gMutexForEnqDeq[sCountThread], NULL );
		/* Condition Value */
		pthread_cond_init( &gCondition[sCountThread], NULL );
		/* Flags */
		gFlagForThread[sCountThread] = 0;
		gFlagForEnqDeq[sCountThread] = 0;
	}
	gMainFlag = 0;        /* Flag */
	qInitQueue( sQueue );     /* 큐 sQueue 초기화 */
	/* 스레드 Mutex Lock */
	pthread_mutex_lock( &gMutexForThread );

	/* 스레드 Create */	
	/* Enqueue 스레드 */
	for( sCountThread = 0; sCountThread < THREAD_COUNT; sCountThread++ )
	{
		sArgEnqueueThread[sCountThread] = (qArgPthread *)malloc( sizeof( qArgPthread ) );
		/* 인자의 스레드 ID는 0, 1, ..., n */
		sArgEnqueueThread[sCountThread]->mQueue     = sQueue;
		sArgEnqueueThread[sCountThread]->mThreadId  = sCountThread;
		
		sIsCreatedThread = pthread_create( &sEnqueueThread[sCountThread],
				NULL,
				&qEnqueue,
				(void *)sArgEnqueueThread[sCountThread] );
		
		if( sIsCreatedThread != 0 )
		{
			printf("[ERROR-001] :: cannot create Addthread.\n");
		}
	}
																		      
	/* Dequeue 스레드 */
	
	for( sCountThread = 0; sCountThread < DELETE_THREAD_COUNT; sCountThread++ )
	
	{
		sArgDequeueThread[sCountThread] = (qArgPthread *)malloc( sizeof( qArgPthread ) );
		/* 인자의 스레드 ID는 0, 1, ..., n */
		sArgDequeueThread[sCountThread]->mQueue     = sQueue;
		sArgDequeueThread[sCountThread]->mThreadId  = sCountThread;
		
		sIsCreatedThread = pthread_create( &sDequeueThread[sCountThread],
				NULL,
				&qDequeue,
				(void *)sArgDequeueThread[sCountThread] );
		
		if( sIsCreatedThread != 0 )
		{
			printf("[ERROR-001] :: cannot create Delete thread.\n");
		}
	}
	/* 모든 스레드 생성 후 플래그 값 확인 */
	while( gMainFlag < sDoubleThreadCount )
	{
		gMainFlag = 0;
		for( sCountThread = 0; sCountThread < THREAD_COUNT; sCountThread++ )
		{
			gMainFlag += gFlagForThread[sCountThread];
		}
	}
	/* 스레드 Mutex Unlock */
	pthread_mutex_unlock( &gMutexForThread );
   
	/* 스레드 Join */																					      
	for( sCountThread = 0; sCountThread < THREAD_COUNT; sCountThread++ )
	{
		/* Enqueue 스레드 */
		sIsJoinedThread = pthread_join( sEnqueueThread[sCountThread],
				(void **)&sReturnValue );
		if( sIsJoinedThread != 0 )
		{
			printf("[ERROR-002] :: cannot join enqueue thread.\n");
		}
	}
	for( sCountThread = 0; sCountThread < DELETE_THREAD_COUNT; sCountThread++ )  
	{ 
		/* Dequeue 스레드 */
		sIsJoinedThread = pthread_join( sDequeueThread[sCountThread],
				(void **)&sReturnValue );
		
		if( sIsJoinedThread != 0 )
		{
			printf("[ERROR-002] :: cannot join dequeue thread.\n");
		}
	}
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: program is Exited.... |\n");
	printf("+-----------------------------------+\n\n");
	return 0;
}

/* Initialize Queue */
void qInitQueue( qQueue * aQueue )
{
	aQueue->head  = NULL;
	aQueue->tail  = NULL;
	aQueue->count = 0;
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: queue is Initialized. |\n");
	printf("+-----------------------------------+\n\n");
}
/* Enqueue */
void  * qEnqueue( void * aArgPthread )
{
	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	qNode       * newNode      = NULL;
	
	int           sInputData    = 0;
	int           sRepeatCount  = 0;
	int           sThreadId     = 0;
	
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue      = sArgPthread->mQueue;
	sThreadId   = sArgPthread->mThreadId;
	
	printf("[#%3d][ENQUEUE_THREAD] is ready.\n", sThreadId );
	gFlagForThread[sThreadId] += 1;
	
	while( sRepeatCount < DATA_MAX_COUNT )
	{
		/* Dequeue 스레드의 준비를 기다린다. */
		while( gFlagForEnqDeq[sThreadId] == 0 ) 
		{
		}
		/* 스레드 Mutex Lock */
		pthread_mutex_lock( &gMutexForThread );
		newNode = (qNode *)malloc( sizeof( qNode ) );
		if( sQueue->count >= THREAD_COUNT /* sMaxBuff */ )
		{
			/* printf("[THREAD_%d]\n[ERROR-003] :: queue is full. [ %d/%d ]\n\n", */
			
			/*        sThreadId, */
			
			/*        DATA_MAX_COUNT, */
			
			/*        DATA_MAX_COUNT ); */
		}
		else
		{
			sInputData                  = rand() % 90 + 10; /* 임의의 데이터 값을 두자리 수로 고정 */
			newNode->data           = sInputData;
			newNode->next = NULL;
			newNode->mEnqueueThreadId  = sThreadId;
			
			printf("[#%3d][ENQUEUE_THREAD] Enqueue DATA : %d( %d/%d ) QUEUE STATUS : [ %d/%d ]\n",
					sThreadId,
					sInputData,
					sRepeatCount+1,
					DATA_MAX_COUNT,
					sQueue->count+1,
					THREAD_COUNT /* sMaxBuff */ );
			
			if( sQueue->count == 0 )
			{
				sQueue->head   = newNode;
				sQueue->tail = newNode;
				newNode->next = NULL;
				newNode->prev=NULL;
			}
																																																           
			else
			{
				sQueue->tail->next = newNode;
				newNode->next=NULL;
				newNode->prev = sQueue->tail;
			}
			sQueue->tail  = newNode;
			sQueue->count++;
			sRepeatCount++;
		}
		/* 스레드 Mutex Unlock */
		pthread_mutex_unlock( &gMutexForThread );
		/* Enqueue Mutex Lock */
		pthread_mutex_lock( &gMutexForEnqDeq[sThreadId] );
		gFlagForEnqDeq[sThreadId] = 0; /* 0으로 다시 초기화 */
		
		/* Dequeue 스레드에게 시그널 */
		pthread_cond_signal( &gCondition[sThreadId] );

		/* Enqueue Mutex Unlock */
		pthread_mutex_unlock( &gMutexForEnqDeq[sThreadId] );
	}
}

/* Dequeue */
void  * qDequeue( void * aArgPthread )
{
	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	int       sOutputData       = 0;
	int       sRepeatCount      = 0;
	int       sThreadId         = 0;
	int       sEnqueueThreadId  = 0;
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue     = sArgPthread->mQueue;
	sThreadId  = sArgPthread->mThreadId;
	
	/* sMaxBuff   = DATA_MAX_COUNT * 2; */
	printf("[#%3d][DEQUEUE_THREAD] is ready.\n", sThreadId );
	gFlagForThread[sThreadId] += 1;
	
	while( sRepeatCount < DATA_MAX_COUNT )
	{
		/* Mutex Lock */
		pthread_mutex_lock( &gMutexForEnqDeq[sThreadId] );
		/* Enqueue 스레드에게 준비가 되었음을 알림 */
		gFlagForEnqDeq[sThreadId] = 1;
		
		/* Enqueue 까지 기다린다. */
		pthread_cond_wait( &gCondition[sThreadId], &gMutexForEnqDeq[sThreadId] );
		
		/* Mutex Unlock */
		pthread_mutex_unlock( &gMutexForEnqDeq[sThreadId] );
		/* 스레드 Mutex Lock */
		pthread_mutex_lock( &gMutexForThread );
		
		if(sQueue->count == 0 )
		{
			/* printf("[THREAD_%d]\n[ERROR-004] :: queue is empty. [ 0/%d ]\n\n", */
			
			/*        sThreadId, */
			
			/*        DATA_MAX_COUNT ); */
		}
		else
		{
			sOutputData       = sQueue->head->data;
			sEnqueueThreadId  = sQueue->head->mEnqueueThreadId;
			printf("[#%3d][DEQUEUE_THREAD] Dequeue DATA : %d( %d/%d ) QUEUE STATUS : [ %d/%d ] data by [#%3d][ENQUEUE_THREAD]\n",
					sThreadId,
					sOutputData,
					sRepeatCount+1,
					DATA_MAX_COUNT,
					sQueue->count-1,
					THREAD_COUNT /* sMaxBuff */,
					sEnqueueThreadId );
			if( sQueue->count == 1 )
			{
				free( sQueue->head );
				sQueue->head  = NULL;
				sQueue->tail   = NULL;
				sQueue->count      = 0;
			}
			else
			{
				sQueue->head = sQueue->head->next;
				free( sQueue->head->prev);
				sQueue->head->prev = NULL;
				sQueue->count--;
			}
			sRepeatCount++;
		}
		pthread_mutex_unlock( &gMutexForThread );
	}
}
