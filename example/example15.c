#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define DATA_MAX_COUNT 5

typedef struct qNode
{
	int data;            
	struct qNode  * prev;         
	struct qNode  * next;      
}qNode;


typedef struct qQueue
{
	qNode* head;   
	qNode* tail;      
	int   count; 
}qQueue;


typedef struct qArgPthread
{	
	qQueue* mQueue;      
}qArgPthread;

qQueue* Init();
//void  qInitQueue(qQueue * aQueue);
void *qEnqueue(void* aArgPthread);
void *qDequeue(void* aArgPthread );

pthread_mutex_t mutex;
pthread_cond_t a_cond;
pthread_cond_t d_cond;
 
int  gFlagForEnqDeq;

int main()
{
	pthread_t     addThread;
	pthread_t     deleteThread;      /* THREAD_COUNT 만큼의 Dequeue 스레드 선언 */
	qArgPthread  *sArgAddThread;   /* THREAD_COUNT 만큼의 인자로 넘겨줄 구조체 */
	qArgPthread  *sArgDeleteThread;   /* THREAD_COUNT 만큼의 인자로 넘겨줄 구조체 */
	
	//qQueue* sQueue=NULL;
	qQueue* sQueue = Init();
	int sIsCreatedThread    = 0;           /* 스레드가 Create되었는지 여부를 알기 위한 변수 */
	int sIsJoinedThread     = 0;           /* 스레드가 Join되었는지 여부를 알기 위한 변수 */
	int sReturnValue        = 0;           /* 스레드가 Join 수행 시 반환하는 값 */
	
	sQueue=(qQueue*)malloc(sizeof(qQueue));
	
	/* Mutex */        
	pthread_mutex_init(&mutex,NULL);
	/* Condition Value */
	pthread_cond_init(&a_cond,NULL);
	pthread_cond_init(&d_cond,NULL);
	
	/* Flags */
	gFlagForEnqDeq = 0;	

//	qInitQueue( sQueue );     /* 큐 sQueue 초기화 */

	/* 스레드 Create */	

	/* Enqueue 스레드 */
	sArgAddThread=(qArgPthread*)malloc( sizeof(qArgPthread));
	sArgAddThread->mQueue=sQueue;
	sIsCreatedThread = pthread_create( &addThread,NULL,&qEnqueue,(void *)sArgAddThread);
	if( sIsCreatedThread != 0 )
	{
		printf("[ERROR-001] :: cannot create Addthread.\n");
	}
	/* Dequeue 스레드 */
	sArgDeleteThread=(qArgPthread*)malloc(sizeof(qArgPthread));
	sArgDeleteThread->mQueue=sQueue;
	sIsCreatedThread = pthread_create(&deleteThread,NULL,&qDequeue,(void *)sArgDeleteThread);
		
	if( sIsCreatedThread != 0 )
	{
		printf("[ERROR-001] :: cannot create Delete thread.\n");
	}


	/* 스레드 Join */																					      
		
	/* Enqueue 스레드 */
		
	sIsJoinedThread = pthread_join(addThread,0/*(void**)&sReturnValue*/);
	if( sIsJoinedThread != 0 )
		{
			printf("[ERROR-002] :: cannot join enqueue thread.\n");
		}
	/* Dequeue 스레드 */
	sIsJoinedThread = pthread_join(deleteThread,/*(void **)&sReturnValue*/0 );
	if( sIsJoinedThread != 0 )
	{
		printf("[ERROR-002] :: cannot join dequeue thread.\n");
	}
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: program is Exited.... |\n");
	printf("+-----------------------------------+\n\n");
	return 0;
}

/* Initialize Queue 
void qInitQueue( qQueue * aQueue )
{
	aQueue->head  = NULL;
	aQueue->tail  = NULL;
	aQueue->count = 0;
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: queue is Initialized. |\n");
	printf("+-----------------------------------+\n\n");
}
*/

qQueue* Init()
{
	qQueue* aQueue = (qQueue*)malloc(sizeof(qQueue));
	aQueue->head  = NULL;
	aQueue->tail = NULL;
	aQueue->count = 0;

	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: queue is Initialized. |\n");
	printf("+-----------------------------------+\n\n");
}

/* Enqueue */
void  * qEnqueue( void * aArgPthread )
{
	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	//qNode       * newNode      = NULL;
	int           sInputData    = 0;
	
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue      = sArgPthread->mQueue;
	
	printf("[ENQUEUE_THREAD] is ready.\n");

/* 스레드 Mutex Lock */
	for(int i=0;i<=DATA_MAX_COUNT;i++){
		pthread_mutex_lock(&mutex);		
		qNode* newNode=(qNode *)malloc( sizeof( qNode ) );
		sInputData=rand() % 90 + 10; /* 임의의 데이터 값을 두자리 수로 고정 */
		newNode->data=sInputData;	
		if( sQueue->head ==NULL )
		{	
			sQueue->head =sQueue->tail= newNode;	
			newNode->next = NULL;
			newNode->prev=NULL;
			printf("[ENQUEUE_THREAD] Enqueue DATA : %d QUEUE STATUS : [ %d/%d ]\n",sInputData,sQueue->count+1,DATA_MAX_COUNT);
	
		}
		else
		{
			if(sQueue->count>DATA_MAX_COUNT-1)
			{
				pthread_cond_wait(&d_cond,&mutex);
			}
			else{
				sQueue->tail->next=newNode;
				newNode->next=NULL;
				newNode->prev=sQueue->tail;
				sQueue->tail=newNode;
				printf("[ENQUEUE_THREAD] Enqueue DATA : %d QUEUE STATUS : [ %d/%d ]\n",sInputData,sQueue->count+1,DATA_MAX_COUNT);
			}
		}
		sQueue->count++;
		gFlagForEnqDeq = 0; /* 0으로 다시 초기화 */
		pthread_cond_signal(&a_cond);
		pthread_mutex_unlock(&mutex);
	}

}



/* Dequeue */
void  * qDequeue( void * aArgPthread )
{
	qNode* cur;
	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	int       sOutputData       = 0;
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue     = sArgPthread->mQueue;
	printf("[DEQUEUE_THREAD] is ready.\n");

	for(int i=0;i<DATA_MAX_COUNT;i++){
		pthread_mutex_lock(&mutex);
		gFlagForEnqDeq = 1;

		if(sQueue->head==NULL)
		{
			pthread_cond_wait(&a_cond,&mutex);
		}
		
		if( sQueue->count==1)
		{
			printf("[DEQUEUE_THREAD] Dequeue DATA : %d  QUEUE STATUS : [ %d/%d ]\n",sQueue->head->data,sQueue->count-1,DATA_MAX_COUNT);
			sQueue->head  = NULL;
			sQueue->tail   = NULL;
			free(sQueue->head);
			sQueue->count--;      
		}
		else
		{

			printf("[DEQUEUE_THREAD] Dequeue DATA : %d  QUEUE STATUS : [ %d/%d ]\n",sQueue->head->data,sQueue->count-1,DATA_MAX_COUNT);
			sQueue->head=sQueue->head->next;
			free(sQueue->head->prev);
			sQueue->head->prev=NULL;
			sQueue->count--;
		}
		//sQueue->count--;
		pthread_cond_signal(&d_cond);
		pthread_mutex_unlock(&mutex);
	}

}
