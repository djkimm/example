#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define THREAD_COUNT 3
#define DELETE_THREAD_COUNT 3 
#define DATA_MAX_COUNT 5

typedef struct qNode
{
	int data;      
	int mEnqueueThreadId;  
	struct qNode  * prev; 
	struct qNode  * next;
} qNode;

typedef struct qQueue
{
	qNode* head;    
	qNode* tail;    
	int   count;  
} qQueue;

typedef struct qArgPthread
{
	qQueue* mQueue;      
	int   mThreadId;  
} qArgPthread;

void  qInitQueue(qQueue * aQueue);
void *qEnqueue(void* aArgPthread);
void *qDequeue(void* aArgPthread );

pthread_mutex_t gMutexForThread;
pthread_mutex_t gMutexForEnqDeq[THREAD_COUNT];
pthread_cond_t gCondition[THREAD_COUNT]; 
 
int  gMainFlag;
int  gFlagForThread[THREAD_COUNT];
int  gDelFlagForThread[DELETE_THREAD_COUNT];
int  gFlagForEnqDeq[THREAD_COUNT];


int main()
{
	pthread_t sEnqueueThread[THREAD_COUNT];    
	pthread_t sDequeueThread[DELETE_THREAD_COUNT];
	qArgPthread* sArgEnqueueThread[THREAD_COUNT];   
	qArgPthread* sArgDequeueThread[DELETE_THREAD_COUNT];
	
	qQueue* sQueue=NULL;
	
	int sIsCreatedThread=0;    
	int sIsJoinedThread=0;   
	int sReturnValue=0;  
	int sCountThread=0;       
	int sDoubleThreadCount=0;
	
	sQueue=(qQueue*)malloc(sizeof(qQueue));
	sDoubleThreadCount = THREAD_COUNT*2;            
	pthread_mutex_init(&gMutexForThread,NULL);
	
	for(sCountThread=0;sCountThread<THREAD_COUNT;sCountThread++)
	{
		/* Mutex */        
		pthread_mutex_init(&gMutexForEnqDeq[sCountThread],NULL);
		/* Condition Value */
		pthread_cond_init(&gCondition[sCountThread],NULL);
		/* Flags */
		gFlagForThread[sCountThread] = 0;
		gFlagForEnqDeq[sCountThread] = 0;
	}
	gMainFlag = 0;       
	qInitQueue(sQueue);     

	pthread_mutex_lock(&gMutexForThread);

	/* Enqueue Thread Create */
	for( sCountThread = 0; sCountThread < THREAD_COUNT; sCountThread++ )
	{
		sArgEnqueueThread[sCountThread] = (qArgPthread *)malloc(sizeof(qArgPthread));
		sArgEnqueueThread[sCountThread]->mQueue=sQueue;
		sArgEnqueueThread[sCountThread]->mThreadId=sCountThread;
		
		sIsCreatedThread = pthread_create(&sEnqueueThread[sCountThread],
				NULL,
				&qEnqueue,
				(void *)sArgEnqueueThread[sCountThread]);
		
		if( sIsCreatedThread != 0 )
		{
			printf("[ERROR-001] :: cannot create Addthread.\n");
		}
	}
																		      
	/* Dequeue Thread Create*/
	for(sCountThread=0; sCountThread<DELETE_THREAD_COUNT;sCountThread++)
	{
		sArgDequeueThread[sCountThread] = (qArgPthread *)malloc(sizeof(qArgPthread));
		sArgDequeueThread[sCountThread]->mQueue=sQueue;
		sArgDequeueThread[sCountThread]->mThreadId=sCountThread;
		
		sIsCreatedThread = pthread_create(&sDequeueThread[sCountThread],
				NULL,
				&qDequeue,
				(void *)sArgDequeueThread[sCountThread]);
		if( sIsCreatedThread != 0 )
		{
			printf("[ERROR-001] :: cannot create Delete thread.\n");
		}
	}
	while( gMainFlag < sDoubleThreadCount )
	{
		gMainFlag = 0;
		for( sCountThread = 0; sCountThread < THREAD_COUNT; sCountThread++ )
		{
			gMainFlag += gFlagForThread[sCountThread];
		}
	}
	pthread_mutex_unlock( &gMutexForThread );
   
	/* Thread Join*/
	for(sCountThread=0;sCountThread<THREAD_COUNT;sCountThread++)
	{
		/* Enqueue 스레드 */
		sIsJoinedThread = pthread_join(sEnqueueThread[sCountThread],
				(void **)&sReturnValue);
		if(sIsJoinedThread != 0)
		{
			printf("[ERROR-002] :: cannot join enqueue thread.\n");
		}
	}
	for(sCountThread = 0;sCountThread<DELETE_THREAD_COUNT;sCountThread++)  
	{ 
		/* Dequeue 스레드 */
		sIsJoinedThread = pthread_join(sDequeueThread[sCountThread],
				(void **)&sReturnValue);
		
		if( sIsJoinedThread != 0 )
		{
			printf("[ERROR-002] :: cannot join dequeue thread.\n");
		}
	}
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: program is Exited.... |\n");
	printf("+-----------------------------------+\n\n");
	return 0;
}

/* Initialize Queue */
void qInitQueue(qQueue* aQueue)
{
	aQueue->head  = NULL;
	aQueue->tail  = NULL;
	aQueue->count = 0;
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: queue is Initialized. |\n");
	printf("+-----------------------------------+\n\n");
}

/* Enqueue */
void  * qEnqueue( void * aArgPthread )
{
	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	qNode       * newNode      = NULL;
	
	int           sInputData    = 0;
	int           sRepeatCount  = 0;
	int           sThreadId     = 0;
	
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue      = sArgPthread->mQueue;
	sThreadId   = sArgPthread->mThreadId;
	
	printf("[#%3d][ENQUEUE_THREAD] is ready.\n", sThreadId );
	gFlagForThread[sThreadId] += 1;
	
	while( sRepeatCount < DATA_MAX_COUNT )
	{
		/* Dequeue 스레드의 준비를 기다린다. */
		while( gFlagForEnqDeq[sThreadId] == 0 ) 
		{
		}
		/* 스레드 Mutex Lock */
		pthread_mutex_lock( &gMutexForThread );
		newNode = (qNode *)malloc( sizeof( qNode ) );
		if( sQueue->count >= THREAD_COUNT)
		{
			//여기에 pthread_cond_wait 넣으면 될 것 같아요 ~~~~~~~~~~~~~~~~
		}
		else
		{
			sInputData=rand()% 90+10;
			newNode->data = sInputData;
			newNode->next = NULL;
			newNode->mEnqueueThreadId = sThreadId;
			printf("[#%3d][ENQUEUE_THREAD] Enqueue DATA : %d QUEUE STATUS : [ %d/%d ]\n",
					sThreadId,
					sInputData,
					sQueue->count+1,
					THREAD_COUNT);
			if(sQueue->count==0)
			{
				sQueue->head   = newNode;
				sQueue->tail = newNode;
				newNode->next = NULL;
				newNode->prev=NULL;
			}
			else
			{
				sQueue->tail->next = newNode;
				newNode->next=NULL;
				newNode->prev = sQueue->tail;
	
			}
			sQueue->tail  = newNode;
			sQueue->count++;
			sRepeatCount++;
		}
		/* 스레드 Mutex Unlock */
		pthread_mutex_unlock(&gMutexForThread);
		/* Enqueue Mutex Lock */
		pthread_mutex_lock(&gMutexForEnqDeq[sThreadId]);
		gFlagForEnqDeq[sThreadId]=0; /* 0으로 다시 초기화 */
		/* Dequeue 스레드에게 시그널 */
		pthread_cond_signal(&gCondition[sThreadId]);
		/* Enqueue Mutex Unlock */
		pthread_mutex_unlock( &gMutexForEnqDeq[sThreadId] );
	}
}

/* Dequeue */
void* qDequeue(void* aArgPthread)
{
	qArgPthread *sArgPthread = NULL;
	qQueue *sQueue=NULL;
	int sOutputData=0;
	int sRepeatCount=0;
	int sThreadId=0;
	int sEnqueueThreadId=0;
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue = sArgPthread->mQueue;
	sThreadId = sArgPthread->mThreadId;
	
	printf("[#%3d][DEQUEUE_THREAD] is ready.\n", sThreadId );
	gFlagForThread[sThreadId] += 1;
	
	while( sRepeatCount < DATA_MAX_COUNT )
	{
		/* Mutex Lock */
		pthread_mutex_lock(&gMutexForEnqDeq[sThreadId]);
		/* Enqueue 스레드에게 준비가 되었음을 알림 */
		gFlagForEnqDeq[sThreadId] = 1;
		
		/* Enqueue 까지 기다린다. */
		pthread_cond_wait(&gCondition[sThreadId],&gMutexForEnqDeq[sThreadId]);
		
		/* Mutex Unlock */
		pthread_mutex_unlock( &gMutexForEnqDeq[sThreadId] );
		/* 스레드 Mutex Lock */
		pthread_mutex_lock(&gMutexForThread);
		
		if(sQueue->count == 0 )
		{
		}
		else
		{
			sOutputData = sQueue->head->data;
			sEnqueueThreadId  = sQueue->head->mEnqueueThreadId;
			printf("[#%3d][DEQUEUE_THREAD] Dequeue DATA : %d QUEUE STATUS : [ %d/%d ] data by [#%3d][ENQUEUE_THREAD]\n",
					sThreadId,
					sOutputData,
					sQueue->count-1,
					THREAD_COUNT,
					sEnqueueThreadId);
			if( sQueue->count == 1)
			{
				free(sQueue->head);
				sQueue->head = NULL;
				sQueue->tail = NULL;
				sQueue->count = 0;
			}
			else
			{
				sQueue->head = sQueue->head->next;
				free(sQueue->head->prev);
				sQueue->head->prev = NULL;
				sQueue->count--;
			}
			sRepeatCount++;
		}
		pthread_mutex_unlock( &gMutexForThread );
	}
}
