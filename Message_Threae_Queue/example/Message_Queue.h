#ifndef __MESSAGE_QUEUE_H__
#define __MESSAGE_QUEUE_H__

#define MAX_COUNT 20

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

typedef struct qNode
{
	int data;      
	int mEnqueueThreadId;  
	struct qNode* prev; 
	struct qNode* next;
} qNode;

typedef struct qQueue
{
	qNode* head;    
	qNode* tail;    
	int   count;  
} qQueue;

typedef struct qArgPthread
{
	qQueue* mQueue;      
	int mThreadId;  
} qArgPthread;

qQueue* Init();
void *qEnqueue(void* aArgPthread);
void *qDequeue(void* aArgPthread);

pthread_mutex_t mutex;
pthread_cond_t a_cond;

#endif
