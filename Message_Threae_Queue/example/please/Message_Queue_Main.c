#include "Message_Queue.h"

int main()
{
	int aThread;
	int dThread;
	int check;

	printf("\nADD_THREAD_COUNT: ");
	check=scanf("%d",&aThread);
	while(getchar()!='\n');
	if(check==0){
		printf("\nOnly Insert Int data\n\n");
		return;
	}

	printf("\nDELETE_THREAD_COUNT: ");
	check=scanf("%d",&dThread);
	while(getchar()!='\n');
	if(check==0){
		printf("\nOnly Insert Int data\n\n");
		return;
	}	
	pthread_t sEnqueueThread[aThread];    
	pthread_t sDequeueThread[dThread];

	qArgPthread* sArgEnqueueThread[aThread];   
	qArgPthread* sArgDequeueThread[dThread];
	
	qQueue* sQueue = Init();
	int sIsCreatedThread=0;    
	int sIsJoinedThread=0;   
	int sReturnValue=0;  
	int sCountThread=0;
	
	sQueue=(qQueue*)malloc(sizeof(qQueue));

	pthread_mutex_init(&mutex,NULL);
	pthread_cond_init(&a_cond,NULL);
	
	//qInitQueue(sQueue);     

	pthread_mutex_lock(&mutex);

	/* Enqueue Thread Create */
	for( sCountThread = 0; sCountThread < aThread; sCountThread++ )
	{
		sArgEnqueueThread[sCountThread] = (qArgPthread *)malloc(sizeof(qArgPthread));
		sArgEnqueueThread[sCountThread]->mQueue=sQueue;
		sArgEnqueueThread[sCountThread]->mThreadId=sCountThread;
		
		sIsCreatedThread = pthread_create(&sEnqueueThread[sCountThread],
				NULL,
				&qEnqueue,
				(void *)sArgEnqueueThread[sCountThread]);
		
		if( sIsCreatedThread != 0 )
		{
			printf("[ERROR-001] :: cannot create Addthread.\n");
		}
	}
																		      
	/* Dequeue Thread Create*/
	for(sCountThread=0; sCountThread<dThread;sCountThread++)
	{
		sArgDequeueThread[sCountThread] = (qArgPthread *)malloc(sizeof(qArgPthread));
		sArgDequeueThread[sCountThread]->mQueue=sQueue;
		sArgDequeueThread[sCountThread]->mThreadId=sCountThread;
		
		sIsCreatedThread = pthread_create(&sDequeueThread[sCountThread],
				NULL,
				&qDequeue,
				(void *)sArgDequeueThread[sCountThread]);
		if( sIsCreatedThread != 0 )
		{
			printf("[ERROR-001] :: cannot create Delete thread.\n");
		}
	}
	pthread_mutex_unlock(&mutex);
   
	/* Thread Join*/
	for(sCountThread=0;sCountThread<aThread;sCountThread++)
	{
		/* Enqueue 스레드 */
		sIsJoinedThread = pthread_join(sEnqueueThread[sCountThread],0);
		if(sIsJoinedThread != 0)
		{
			printf("[ERROR-002] :: cannot join enqueue thread.\n");
		}
	}
	for(sCountThread = 0;sCountThread<dThread;sCountThread++)  
	{ 
		/* Dequeue 스레드 */
		sIsJoinedThread = pthread_join(sDequeueThread[sCountThread],0);
		
		if( sIsJoinedThread != 0 )
		{
			printf("[ERROR-002] :: cannot join dequeue thread.\n");
		}
	}
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: program is Exited.... |\n");
	printf("+-----------------------------------+\n\n");
	return 0;
}

