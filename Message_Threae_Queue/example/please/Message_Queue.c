#include "Message_Queue.h"
int aThread;
int dThread;
/* Initialize Queue */
qQueue* Init()
{
	qQueue* aQueue = (qQueue*)malloc(sizeof(qQueue));
	aQueue->head = NULL;
	aQueue->tail = NULL;
	aQueue->count = 0;

	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: queue is Initialized. |\n");
	printf("+-----------------------------------+\n\n");
}

/* Enqueue */
void  *qEnqueue(void* aArgPthread)
{
	qArgPthread* sArgPthread = NULL;
	qQueue* sQueue = NULL;
	qNode* newNode = NULL;
	
	int sInputData = 0;
	int sRepeatCount = 0;
	int sThreadId = 0;
	
	sArgPthread = (qArgPthread*)aArgPthread;
	sQueue = sArgPthread->mQueue;
	sThreadId = sArgPthread->mThreadId;
	
	printf("[#%3d][ENQUEUE_THREAD] is ready.\n", sThreadId);
	
	while(sRepeatCount< MAX_COUNT+1)
	{
		/* 스레드 Mutex Lock */
		pthread_mutex_lock(&mutex);
		newNode = (qNode *)malloc( sizeof( qNode ) );
		sInputData=rand()% 90+10;
		newNode->data = sInputData;
		newNode->next = NULL;
		newNode->mEnqueueThreadId = sThreadId;
		printf("[#%3d][ENQUEUE_THREAD] Enqueue DATA : %d QUEUE STATUS : [ %d/%d ]\n",
				sThreadId,
				sInputData,
				sQueue->count+1,
				1000
				);
		if(sQueue->count==0)
		{
			sQueue->head = newNode;
			sQueue->tail = newNode;
			newNode->next = NULL;
			newNode->prev=NULL;
		}
		else
		{
			sQueue->tail->next = newNode;
			newNode->next=NULL;
			newNode->prev = sQueue->tail;
		}
		sQueue->tail = newNode;
		sQueue->count++;
		sRepeatCount++;
		pthread_cond_broadcast(&a_cond);
		pthread_mutex_unlock(&mutex);
		usleep(1);
	}
}

/* Dequeue */
void* qDequeue(void* aArgPthread)
{
	qArgPthread *sArgPthread = NULL;
	qQueue *sQueue=NULL;
	int sOutputData=0;
	int sRepeatCount=0;
	int sThreadId=0;
	int sEnqueueThreadId=0;
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue = sArgPthread->mQueue;
	sThreadId = sArgPthread->mThreadId;
	
	printf("[#%3d][DEQUEUE_THREAD] is ready.\n", sThreadId );
	
	if(aThread>=dThread)
	{
		while(sRepeatCount<MAX_COUNT)
		{
			pthread_mutex_lock(&mutex);
			while(sQueue->head ==NULL && sRepeatCount<MAX_COUNT)
			{
				if(sQueue->count ==0)
				{
					pthread_cond_wait(&a_cond,&mutex);
				}
			}
			if(sQueue->count != 0)
			{
				sOutputData = sQueue->head->data;
				sEnqueueThreadId  = sQueue->head->mEnqueueThreadId;
				printf("[#%3d][DEQUEUE_THREAD] Dequeue DATA : %d QUEUE STATUS : [ %d/%d ] data by [#%3d][ENQUEUE_THREAD]\n",
						sThreadId,
						sOutputData,
						sQueue->count-1,
						1000,
						sEnqueueThreadId);
				if( sQueue->count == 1)
				{
					free(sQueue->head);
					sQueue->head = NULL;
					sQueue->tail = NULL;
					sQueue->count = 0;
				}
				else
				{
					sQueue->head = sQueue->head->next;
					free(sQueue->head->prev);
					sQueue->head->prev = NULL;
					sQueue->count--;
				}
				sRepeatCount++;
			}
			else{
				printf("\n Queue is empty.\n\n");
				sRepeatCount++;
				pthread_mutex_unlock(&mutex);
				break;
			}
			pthread_mutex_unlock( &mutex);
			usleep(1);
		}
	}
	else
	{
		while(sRepeatCount<MAX_COUNT)
		{
			pthread_mutex_lock(&mutex);
			while(sQueue->head ==NULL && sRepeatCount<MAX_COUNT/dThread)
			{
				if(sQueue->count ==0)
				{
					pthread_cond_wait(&a_cond,&mutex);
				}
			}
			if(sQueue->count != 0)
			{
				sOutputData = sQueue->head->data;
				sEnqueueThreadId  = sQueue->head->mEnqueueThreadId;
				printf("[#%3d][DEQUEUE_THREAD] Dequeue DATA : %d QUEUE STATUS : [ %d/%d ] data by [#%3d][ENQUEUE_THREAD]\n",
						sThreadId,
						sOutputData,
						sQueue->count-1,
						1000,
						sEnqueueThreadId);
				if( sQueue->count == 1)
				{
					free(sQueue->head);
					sQueue->head = NULL;
					sQueue->tail = NULL;
					sQueue->count = 0;
				}
				else
				{
					sQueue->head = sQueue->head->next;
					free(sQueue->head->prev);
					sQueue->head->prev = NULL;
					sQueue->count--;
				}
				sRepeatCount++;
			}
			else{
				printf("\n Queue is empty.\n\n");
				sRepeatCount++;
				pthread_mutex_unlock(&mutex);
				break;
			}
			pthread_mutex_unlock( &mutex);
			usleep(1);
		}
	}

}




