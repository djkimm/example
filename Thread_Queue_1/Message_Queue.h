#ifndef __MESSAGE_QUEUE_H__
#define __MESSAGE_QUEUE_H__

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define DATA_MAX_COUNT 5

typedef struct qNode
{
	int data;            
	struct qNode* prev;         
	struct qNode* next;      
}qNode;

typedef struct qQueue
{
	qNode* head;   
	qNode* tail;      
	int count; 
}qQueue;

typedef struct qArgPthread
{	
	qQueue* mQueue;      
}qArgPthread;

qQueue* Init();
void *qEnqueue(void* aArgPthread);
void *qDequeue(void* aArgPthread);

pthread_mutex_t mutex;
pthread_cond_t a_cond;
pthread_cond_t d_cond;

#endif
