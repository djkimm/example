#include "Message_Queue.h"

qQueue* Init()
{
	qQueue* aQueue = (qQueue*)malloc(sizeof(qQueue));
	aQueue->head  = NULL;
	aQueue->tail = NULL;
	aQueue->count = 0;

	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: queue is Initialized. |\n");
	printf("+-----------------------------------+\n\n");
}

/* Enqueue */
void  * qEnqueue( void * aArgPthread )
{
	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	int           sInputData    = 0;
	
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue      = sArgPthread->mQueue;
	
	printf("[ENQUEUE_THREAD] is ready.\n");
	for(int i=0;i<=12;i++){
		pthread_mutex_lock(&mutex);		
		qNode* newNode=(qNode *)malloc( sizeof( qNode ) );
		sInputData=rand() % 90 + 10; /* 임의의 데이터 값을 두자리 수로 고정 */
		newNode->data=sInputData;	
		newNode->next=NULL;
		if( sQueue->count >DATA_MAX_COUNT-1 )
		{
			printf("Wait_DELETE_NODE\n");
			pthread_cond_wait(&d_cond,&mutex);
		}
		else
		{
			printf("[ENQUEUE_THREAD] Enqueue DATA : %d QUEUE STATUS : [ %d/%d ]\n",sInputData,sQueue->count+1,DATA_MAX_COUNT);
			if(sQueue->count==0)
			{
				sQueue->head=newNode;
				newNode->prev =NULL;
			}
			else{
				sQueue->tail->next=newNode;
				newNode->prev=sQueue->tail;
			}
			sQueue->tail = newNode;
			sQueue->count++;
		}
		pthread_cond_signal(&a_cond);
		pthread_mutex_unlock(&mutex);
	}

}

/* Dequeue */
void  * qDequeue( void * aArgPthread )
{
	qNode* cur;
	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	int       sOutputData       = 0;
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue     = sArgPthread->mQueue;
	printf("[DEQUEUE_THREAD] is ready.\n");

	for(int i=0;i<=11;i++){
		pthread_mutex_lock(&mutex);
		if(sQueue->head==NULL)
		{
			printf("Wait_Add_Node\n");
			pthread_cond_wait(&a_cond,&mutex);
		}
		else
		{
			printf("[DEQUEUE_THREAD] Dequeue DATA : %d  QUEUE STATUS : [ %d/%d ]\n",sQueue->head->data,sQueue->count-1,DATA_MAX_COUNT);
			if(sQueue->count==1)
			{
				free(sQueue->head);
				sQueue->head  = NULL;
				sQueue->tail   = NULL;
			}
			else
			{
				sQueue->head=sQueue->head->next;
				free(sQueue->head->prev);
				sQueue->head->prev=NULL;
			}
			sQueue->count--;
		}
			pthread_cond_signal(&d_cond);
			pthread_mutex_unlock(&mutex);
	}

}

