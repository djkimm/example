#include "Message_Queue.h"


int main()
{
	pthread_t     addThread;
	pthread_t     deleteThread;  
	qArgPthread  *sArgAddThread;  
	qArgPthread  *sArgDeleteThread;   
	
	qQueue* sQueue = Init();
	int sIsCreatedThread    = 0;           
	int sIsJoinedThread     = 0;          
	int sReturnValue        = 0;       
	
	sQueue=(qQueue*)malloc(sizeof(qQueue));
	
	pthread_mutex_init(&mutex,NULL);
	/* Condition Value */
	pthread_cond_init(&a_cond,NULL);
	pthread_cond_init(&d_cond,NULL);
	
	/* Thread Create */	
	/* Enqueue 스레드 */
	sArgAddThread=(qArgPthread*)malloc( sizeof(qArgPthread));
	sArgAddThread->mQueue=sQueue;
	sIsCreatedThread = pthread_create( &addThread,NULL,&qEnqueue,(void *)sArgAddThread);
	if( sIsCreatedThread != 0 )
	{
		printf("[ERROR-001] :: cannot create Addthread.\n");
	}
	/* Dequeue 스레드 */
	sArgDeleteThread=(qArgPthread*)malloc(sizeof(qArgPthread));
	sArgDeleteThread->mQueue=sQueue;
	sIsCreatedThread = pthread_create(&deleteThread,NULL,&qDequeue,(void *)sArgDeleteThread);
		
	if( sIsCreatedThread != 0 )
	{
		printf("[ERROR-001] :: cannot create Delete thread.\n");
	}

	/* 스레드 Join */																					      
	/* Enqueue 스레드 */
	sIsJoinedThread = pthread_join(addThread,0);
	if( sIsJoinedThread != 0 )
		{
			printf("[ERROR-002] :: cannot join enqueue thread.\n");
		}
	/* Dequeue 스레드 */
	sIsJoinedThread = pthread_join(deleteThread,0);
	if( sIsJoinedThread != 0 )
	{
		printf("[ERROR-002] :: cannot join dequeue thread.\n");
	}
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: program is Exited.... |\n");
	printf("+-----------------------------------+\n\n");
	return 0;
}
