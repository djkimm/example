#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define DATA_MAX_COUNT 20 

typedef struct qNode
{
	int data;            
	struct qNode  * prev;         
	struct qNode  * next;      
}qNode;

typedef struct qQueue
{
	qNode* head;   
	qNode* tail;      
	int   count; 
}qQueue;

typedef struct qArgPthread
{	
	qQueue* mQueue;      
}qArgPthread;

qQueue* Init();
void *qEnqueue(void* aArgPthread);
void *qDequeue(void* aArgPthread );

pthread_mutex_t mutex;
pthread_cond_t a_cond;
pthread_cond_t d_cond;

int main()
{
	pthread_t     addThread;
	pthread_t     deleteThread;  
	qArgPthread  *sArgAddThread;  
	qArgPthread  *sArgDeleteThread;   
	
	qQueue* sQueue = Init();
	int sIsCreatedThread    = 0;           
	int sIsJoinedThread     = 0;          
	int sReturnValue        = 0;       
	
	sQueue=(qQueue*)malloc(sizeof(qQueue));
	
	pthread_mutex_init(&mutex,NULL);
	/* Condition Value */
	pthread_cond_init(&a_cond,NULL);
	pthread_cond_init(&d_cond,NULL);
	
	/* Thread Create */	
	/* Enqueue 스레드 */
	sArgAddThread=(qArgPthread*)malloc( sizeof(qArgPthread));
	sArgAddThread->mQueue=sQueue;
	sIsCreatedThread = pthread_create( &addThread,NULL,&qEnqueue,(void *)sArgAddThread);
	if( sIsCreatedThread != 0 )
	{
		printf("[ERROR-001] :: cannot create Addthread.\n");
	}
	/* Dequeue 스레드 */
	sArgDeleteThread=(qArgPthread*)malloc(sizeof(qArgPthread));
	sArgDeleteThread->mQueue=sQueue;
	sIsCreatedThread = pthread_create(&deleteThread,NULL,&qDequeue,(void *)sArgDeleteThread);
		
	if( sIsCreatedThread != 0 )
	{
		printf("[ERROR-001] :: cannot create Delete thread.\n");
	}

	/* 스레드 Join */																					      
	/* Enqueue 스레드 */
	sIsJoinedThread = pthread_join(addThread,0);
	if( sIsJoinedThread != 0 )
		{
			printf("[ERROR-002] :: cannot join enqueue thread.\n");
		}
	/* Dequeue 스레드 */
	sIsJoinedThread = pthread_join(deleteThread,0);
	if( sIsJoinedThread != 0 )
	{
		printf("[ERROR-002] :: cannot join dequeue thread.\n");
	}
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: program is Exited.... |\n");
	printf("+-----------------------------------+\n\n");
	return 0;
}
qQueue* Init()
{
	qQueue* aQueue = (qQueue*)malloc(sizeof(qQueue));
	aQueue->head  = NULL;
	aQueue->tail = NULL;
	aQueue->count = 0;

	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: queue is Initialized. |\n");
	printf("+-----------------------------------+\n\n");
}

/* Enqueue */
void  * qEnqueue( void * aArgPthread )
{
	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	int           sInputData    = 0;
	int sRepeatCount =0;
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue      = sArgPthread->mQueue;
	
	printf("[ENQUEUE_THREAD] is ready.\n");
	while(sRepeatCount<100)
	{
		pthread_mutex_lock(&mutex);

		qNode* newNode=(qNode *)malloc( sizeof( qNode ) );
		sInputData=rand() % 90 + 10; /* 임의의 데이터 값을 두자리 수로 고정 */
		newNode->data=sInputData;	
		newNode->next=NULL;
	
		printf("[ENQUEUE_THREAD] Enqueue DATA : %d QUEUE STATUS : [ %d/%d ]\n",sInputData,sQueue->count+1,DATA_MAX_COUNT);

		if(sQueue->count>=DATA_MAX_COUNT-1)
		{
			return NULL;
		}
		else{
	
			if(sQueue->count==0)
			{
				sQueue->head=newNode;
				newNode->prev =NULL;
			}
			else{
				sQueue->tail->next=newNode;
				newNode->prev=sQueue->tail;
			}
			sQueue->tail = newNode;
			sQueue->count++;
			sRepeatCount ++;
		}
		pthread_cond_signal(&a_cond);
		pthread_mutex_unlock(&mutex);
		usleep(1);
	}
}

/* Dequeue */
void  * qDequeue( void * aArgPthread )
{
	qNode* cur;
	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	int       sOutputData       = 0;
	int sRepeatCount =0;
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue     = sArgPthread->mQueue;
	printf("[DEQUEUE_THREAD] is ready.\n");

	while(sRepeatCount<100)
	{
		pthread_mutex_lock(&mutex);
		while(sQueue->count==0)
		{
			if(sQueue->head==NULL)
			{
				pthread_cond_wait(&a_cond,&mutex);
			}
		}
		if(sQueue->count>0)
		{
			printf("[DEQUEUE_THREAD] Dequeue DATA : %d QUEUE STATUS : [ %d/%d ]\n",sQueue->head->data,sQueue->count-1,DATA_MAX_COUNT);
			if(sQueue->count==1)
				{
					free(sQueue->head);
					sQueue->head  = NULL;
					sQueue->tail   = NULL;
				}
			else
			{
				sQueue->head=sQueue->head->next;
				free(sQueue->head->prev);
				sQueue->head->prev=NULL;
			}
			sQueue->count--;
		}
		sRepeatCount++;
		pthread_mutex_unlock(&mutex);
	}

}





