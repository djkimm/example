#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


#define ADD_THREAD_COUNT    3
#define DELETE_THREAD_COUNT   3
#define DATA_MAX_COUNT  2

/* 큐를 이루는 노드 구조체 */
typedef struct qNode
{
	int             mData;             /* Data */
	int             mEnqueueThreadId;  /* 데이터를 준 스레드 ID */
	struct qNode  * mPrevNode;         /* 이전 Node */
	struct qNode  * mNextNode;         /* 다음 Node */
} qNode;


/* 큐 구조체 */
typedef struct qQueue
{
	qNode  * mFrontNode;      /* Dequeue */
	qNode  * mRearNode;       /* Enqueue */
	int      mCount;          /* 노드 갯수 */
} qQueue;

/* 스레드 생성 시 인자로 넘겨주는 구조체 */
typedef struct qArgPthread
{
	qQueue  * mQueue;         /* Queue */
	int       mThreadId;      /* 스레드 ID */
} qArgPthread;

/* Initialize Queue */
void  qInitQueue( qQueue * aQueue );


/* Enqueue */
void  * qEnqueue( void * aArgPthread );


/* Dequeue */
void  * qDequeue( void * aArgPthread );



/* Global Mutex & Condition Variable */
pthread_mutex_t  gMutexForThread;
pthread_mutex_t  gMutexForEnqDeq[ADD_THREAD_COUNT];

pthread_cond_t   gCondition[ADD_THREAD_COUNT]; //아직 확실하지 않음 ..
 

/* Flag Variables */
int  gAddMainFlag;
int  gDelMainFlag;
int  gAddFlagForThread[ADD_THREAD_COUNT];
int  gDelFlagForThread[DELETE_THREAD_COUNT];
int  gFlagForEnqDeq[ADD_THREAD_COUNT];


int main()
{

	pthread_t      sEnqueueThread[ADD_THREAD_COUNT];      /* THREAD_COUNT 만큼의 Enqueue 스레드 선언 */
	pthread_t      sDequeueThread[DELETE_THREAD_COUNT];      /* THREAD_COUNT 만큼의 Dequeue 스레드 선언 */
	qArgPthread  * sArgEnqueueThread[ADD_THREAD_COUNT];   /* THREAD_COUNT 만큼의 인자로 넘겨줄 구조체 */
	qArgPthread  * sArgDequeueThread[DELETE_THREAD_COUNT];   /* THREAD_COUNT 만큼의 인자로 넘겨줄 구조체 */
	qQueue       * sQueue              = NULL;
	int            sIsCreatedThread    = 0;           /* 스레드가 Create되었는지 여부를 알기 위한 변수 */
	int            sIsJoinedThread     = 0;           /* 스레드가 Join되었는지 여부를 알기 위한 변수 */
	int            sReturnValue        = 0;           /* 스레드가 Join 수행 시 반환하는 값 */
	int            sAddCountThread     = 0;           /* 스레드 개수, 각종 for문의 정수형 변수 */
	int          sDelCountThread     = 0;
	int            sAddDoubleThreadCount = 0;
	int          sDelDoubleThreadCount = 0;
	
	sQueue  = (qQueue *)malloc( sizeof( qQueue ) );
	
	sAddDoubleThreadCount = ADD_THREAD_COUNT * 2;            /* THREAD_COUNT의 두배 */
	sDelDoubleThreadCount = DELETE_THREAD_COUNT * 2;
	
	pthread_mutex_init( &gMutexForThread, NULL );
	for( sAddCountThread = 0; sAddCountThread < ADD_THREAD_COUNT; sAddCountThread++ )
	{
		pthread_mutex_init( &gMutexForEnqDeq[sAddCountThread], NULL );
		pthread_cond_init( &gCondition[sAddCountThread], NULL );
		gAddFlagForThread[sAddCountThread] = 0;
		gFlagForEnqDeq[sAddCountThread] = 0;
	}
	gAddMainFlag = 0;          
	gDelMainFlag = 0;
	qInitQueue( sQueue );     
	
	pthread_mutex_lock( &gMutexForThread );
	
	for( sAddCountThread = 0; sAddCountThread < ADD_THREAD_COUNT; sAddCountThread++ )
	{
		sArgEnqueueThread[sAddCountThread] = (qArgPthread *)malloc( sizeof( qArgPthread ) );
		sArgEnqueueThread[sAddCountThread]->mQueue     = sQueue;
		sArgEnqueueThread[sAddCountThread]->mThreadId  = sAddCountThread;
		
		sIsCreatedThread = pthread_create( &sEnqueueThread[sAddCountThread],
				NULL,
				&qEnqueue,
				(void *)sArgEnqueueThread[sAddCountThread] );
		if( sIsCreatedThread != 0 )
		{
			printf("[ERROR-001] :: cannot create Addthread.\n");
		}
	}
	for( sDelCountThread = 0; sDelCountThread < DELETE_THREAD_COUNT; sDelCountThread++ )
	{
		sArgDequeueThread[sDelCountThread] = (qArgPthread *)malloc( sizeof( qArgPthread ) );
		sArgDequeueThread[sDelCountThread]->mQueue     = sQueue;
		sArgDequeueThread[sDelCountThread]->mThreadId  = sDelCountThread;
		sIsCreatedThread = pthread_create( &sDequeueThread[sDelCountThread],
				NULL,
				&qDequeue,
				(void *)sArgDequeueThread[sDelCountThread] );
		if( sIsCreatedThread != 0 )
		{
			printf("[ERROR-001] :: cannot create Delete thread.\n");
		}
	}
	
	while( gAddMainFlag < sAddDoubleThreadCount )
	{
		gAddMainFlag = 0;
		for( sAddCountThread = 0; sAddCountThread < ADD_THREAD_COUNT; sAddCountThread++ )
		{
			gAddMainFlag += gAddFlagForThread[sAddCountThread];
		}
	}
	while( gDelMainFlag < sDelDoubleThreadCount )
	{
		gDelMainFlag = 0;
		for( sDelCountThread = 0; sDelCountThread < DELETE_THREAD_COUNT; sDelCountThread++ )
		{
			gDelMainFlag += gDelFlagForThread[sDelCountThread];
		}
	}
	pthread_mutex_unlock( &gMutexForThread );
	
	for( sAddCountThread = 0; sAddCountThread < ADD_THREAD_COUNT; sAddCountThread++ )
	{
		sIsJoinedThread = pthread_join( sEnqueueThread[sAddCountThread],
		
				(void **)&sReturnValue );
		
		if( sIsJoinedThread != 0 )
		{
			printf("[ERROR-002] :: cannot join enqueue thread.\n");

		}
		
	}
	for( sDelCountThread = 0; sDelCountThread < DELETE_THREAD_COUNT; sDelCountThread++ )  
	{ 
		sIsJoinedThread = pthread_join( sDequeueThread[sDelCountThread],
				
				(void **)&sReturnValue );

		if( sIsJoinedThread != 0 )
		{
			printf("[ERROR-002] :: cannot join dequeue thread.\n");
		}
	}
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: program is Exited.... |\n");
	printf("+-----------------------------------+\n\n");
	return 0;
}

void qInitQueue( qQueue * aQueue )
{
	aQueue->mFrontNode  = NULL;
	aQueue->mRearNode   = NULL;
	aQueue->mCount      = 0;
	printf("\n+-----------------------------------+");
	printf("\n| [SYSTEM] :: queue is Initialized. |\n");
	printf("+-----------------------------------+\n\n");
}




void  * qEnqueue( void * aArgPthread )
{
	
	qArgPthread * sArgPthread   = NULL;
	
	qQueue      * sQueue        = NULL;
	
	qNode       * sNewNode      = NULL;


	
	
	int           sInputData    = 0;
	
	int           sRepeatCount  = 0;
	
	int           sAThreadId     = 0;
	



	
	
	sArgPthread = (qArgPthread *)aArgPthread;


	
	sQueue      = sArgPthread->mQueue;
	
	sAThreadId   = sArgPthread->mThreadId;

	



	
	printf("[#%3d][ENQUEUE_THREAD] is ready.\n", sAThreadId );
	
	gAddFlagForThread[sAThreadId] += 1;
	

	
	while( sRepeatCount < DATA_MAX_COUNT )
	
	{


		
		while( gFlagForEnqDeq[sAThreadId] == 0 ) 
		
		
		{
		
			sleep(0);
			
		}
																		        
		

		
		pthread_mutex_lock( &gMutexForThread );


		
		sNewNode = (qNode *)malloc( sizeof( qNode ) );
		


		
		if( sQueue->mCount >= ADD_THREAD_COUNT /* sMaxBuff */ )
		
		{
		

			
		}
		
		else
		
		{
		
			sInputData                  = rand() % 90 + 10;
			
			sNewNode->mData             = sInputData;
			
			sNewNode->mNextNode         = NULL;
			
			sNewNode->mEnqueueThreadId  = sAThreadId;


			
			printf("[#%3d][ENQUEUE_THREAD] Enqueue DATA : %d( %d/%d ) QUEUE STATUS : [ %d/%d ]\n",
			
					
					sAThreadId,
					
					sInputData,
					
					sRepeatCount+1,
					
					DATA_MAX_COUNT,
					
					sQueue->mCount+1,
					
					ADD_THREAD_COUNT );



			
			if( sQueue->mCount == 0 )
			
			{
			
				sQueue->mFrontNode   = sNewNode;
				
				sNewNode->mPrevNode  = NULL;
				
			}
			
			else
			
			{
			
				sQueue->mRearNode->mNextNode  = sNewNode;
				
				sNewNode->mPrevNode           = sQueue->mRearNode;
				
			}



			
			sQueue->mRearNode  = sNewNode;
			
			sQueue->mCount++;


			
			sRepeatCount++;
			
		}


		

		
		
		pthread_mutex_unlock( &gMutexForThread );


		
		pthread_mutex_lock( &gMutexForEnqDeq[sAThreadId] );


		
		gFlagForEnqDeq[sAThreadId] = 0;
		

		

		
		
		pthread_cond_signal( &gCondition[sAThreadId] );


		

		
		pthread_mutex_unlock( &gMutexForEnqDeq[sAThreadId] );


		

		
	}
}










void  * qDequeue( void * aArgPthread )
{

	qArgPthread * sArgPthread   = NULL;
	qQueue      * sQueue        = NULL;
	
	int       sOutputData       = 0;
	int       sRepeatCount      = 0;
	int       sDThreadId         = 0;
	int       sEnqueueThreadId  = 0;
	
	sArgPthread = (qArgPthread *)aArgPthread;
	sQueue     = sArgPthread->mQueue;
	sDThreadId  = sArgPthread->mThreadId;
	
	printf("[#%3d][DEQUEUE_THREAD] is ready.\n", sDThreadId );
	gDelFlagForThread[sDThreadId] += 1;
										
	while( sRepeatCount < DATA_MAX_COUNT )
	{
		pthread_mutex_lock( &gMutexForEnqDeq[sDThreadId] );
		gFlagForEnqDeq[sDThreadId] = 1;
		pthread_cond_wait( &gCondition[sDThreadId], &gMutexForEnqDeq[sDThreadId] );
		pthread_mutex_unlock( &gMutexForEnqDeq[sDThreadId] );
		pthread_mutex_lock( &gMutexForThread );
		
		if( sQueue->mCount == 0 )
		{
		}
		else
		{
			sOutputData       = sQueue->mFrontNode->mData;
			sEnqueueThreadId  = sQueue->mFrontNode->mEnqueueThreadId;
			printf("[#%3d][DEQUEUE_THREAD] Dequeue DATA : %d( %d/%d ) QUEUE STATUS : [ %d/%d ] data by [#%3d][ENQUEUE_THREAD]\n",
					sDThreadId,
					sOutputData,
					sRepeatCount+1,
					DATA_MAX_COUNT,
					sQueue->mCount-1,
					DELETE_THREAD_COUNT /* sMaxBuff */,
					sEnqueueThreadId );
			if( sQueue->mCount == 1 )
			{
				free( sQueue->mFrontNode );
				sQueue->mFrontNode  = NULL;
				sQueue->mRearNode   = NULL;
				sQueue->mCount      = 0;
			}
			else
			{
				sQueue->mFrontNode = sQueue->mFrontNode->mNextNode;
				free( sQueue->mFrontNode->mPrevNode );
				sQueue->mFrontNode->mPrevNode = NULL;
				sQueue->mCount--;
			}
			sRepeatCount++;
		}
		pthread_mutex_unlock( &gMutexForThread );

	}
}






